package matlik.measures

import org.scalatest.WordSpec
import org.scalatest.Matchers
import org.junit.runner.RunWith
import scala.util.Try
import scala.util.Failure

@RunWith(classOf[org.scalatest.junit.JUnitRunner])
class MoneySpec extends WordSpec with Matchers {

  "Money" should {
    import SimpleImplicits.USD
    import scala.math.BigDecimal.RoundingMode._

    object TEST_CURR extends Currency {
      val longLabel = "Test Currency"
      val shortLabel = "TEST_CURR"
      val symbol = "$$"
      val significantDecimalDigits = 2
      val minimumValue = BigDecimal("0.05")
    }

    "support basic math" in {
      val base = Money(1.00, USD)
      assertResult(Money(1.02, USD)) { base + 0.02 }
      assertResult(Money(0.98, USD)) { base - 0.02 }
      assertResult(Money(0.02, USD)) { base * 0.02 }
      assertResult(Money(50, USD)) { base / 0.02 }
    }

    "allow compatible currencies to add/subtract" in {
      import SimpleImplicits.SimpleConverter
      val a = Money(5.25, USD)
      val b = Money(1.05, USD)
      assertResult(Money(6.30, USD)) { a + b }
      assertResult(Money(4.20, USD)) { a - b }
    }

    "not allow add/subtract of incompatible currencies" in {
      import SimpleImplicits.SimpleConverter
      val a = Money(1, USD)
      val b = Money(1, TEST_CURR)
      
      Try(a + b) match {
        case Failure(_) => // expected
        case _ => fail()
      }
      
      Try(a - b) match {
        case Failure(_) => // expected
        case _ => fail()
      }
    }
    
    "performs decimal rounding (min of 0.01) accurately" in {
      val base1 = Money(0.125, USD)
      assertResult(Money(0.13, USD)) { base1.round(UP) }
      assertResult(Money(0.12, USD)) { base1.round(DOWN) }
      assertResult(Money(0.13, USD)) { base1.round(CEILING) }
      assertResult(Money(0.12, USD)) { base1.round(FLOOR) }
      assertResult(Money(0.13, USD)) { base1.round(HALF_UP) }
      assertResult(Money(0.12, USD)) { base1.round(HALF_DOWN) }
      Try(base1.round(UNNECESSARY)) match {
        case Failure(_) => // expected
        case _ => fail("Exception expecected, but not found")
      }

      val base2 = base1 * -1
      assertResult(Money(-0.13, USD)) { base2.round(UP) }
      assertResult(Money(-0.12, USD)) { base2.round(DOWN) }
      assertResult(Money(-0.12, USD)) { base2.round(CEILING) }
      assertResult(Money(-0.13, USD)) { base2.round(FLOOR) }
      assertResult(Money(-0.13, USD)) { base2.round(HALF_UP) }
      assertResult(Money(-0.12, USD)) { base2.round(HALF_DOWN) }
      
      // Defaults
      assertResult(Money(0.01, USD)) { Money(0.005, USD).round() }
      assertResult(Money(0.00, USD)) { Money(0.004, USD).round() }
      assertResult(Money(-0.01, USD)) { Money(-0.005, USD).round() }
      assertResult(Money(0.00, USD)) { Money(-0.004, USD).round() }
      
      // Override rounding to nearest dollar
      assertResult(Money(1.00, USD)) { Money(0.50, USD).round(minimumValue = 1, precision = 0) }
      assertResult(Money(0.00, USD)) { Money(0.49, USD).round(minimumValue = 1, precision = 0) }
    }

    """performs "Swedish rounding" where smallest denomination is not decimal""" in {
      // Supports rounding rules as described here => http://en.wikipedia.org/wiki/Swedish_rounding
      val five = Money(0.125, TEST_CURR)
      assertResult(Money(0.15, TEST_CURR)) { five.round(UP) }
      assertResult(Money(0.10, TEST_CURR)) { five.round(DOWN) }
      assertResult(Money(0.15, TEST_CURR)) { five.round(CEILING) }
      assertResult(Money(0.10, TEST_CURR)) { five.round(FLOOR) }
      assertResult(Money(0.15, TEST_CURR)) { five.round(HALF_UP) }
      assertResult(Money(0.10, TEST_CURR)) { five.round(HALF_DOWN) }
      Try(five.round(UNNECESSARY)) match {
        case Failure(_) => // expected
        case _ => fail("Exception expecected, but not found")
      }

      val five2 = Money(-0.125, TEST_CURR)
      assertResult(Money(-0.15, TEST_CURR)) { five2.round(UP) }
      assertResult(Money(-0.10, TEST_CURR)) { five2.round(DOWN) }
      assertResult(Money(-0.10, TEST_CURR)) { five2.round(CEILING) }
      assertResult(Money(-0.15, TEST_CURR)) { five2.round(FLOOR) }
      assertResult(Money(-0.15, TEST_CURR)) { five2.round(HALF_UP) }
      assertResult(Money(-0.10, TEST_CURR)) { five2.round(HALF_DOWN) }
    }
    
    "support comparison functions" in {
      import SimpleImplicits.USD_Dollars
      
      val one = Money(1, USD)
      val oneA = Money(1, USD)
      val two = Money(2, USD)
      
      assert(one < two)
      
      // These are actually equality, not comparison, which is why we cannot support conversion using < style comparison.
      assert(one == oneA)
      assert(one != Money(1, USD_Dollars))
      
      Try(Money(1.5, USD) < Money(1.5, USD_Dollars)) match {
        case Failure(_) => // Expected
        case _ => fail("Should not be able to compare differend currencies")
      }
    }

    "support relation to zero checks" in {
      assert(!Money(0, USD).isNegative)
      assert(Money(0, USD).isZero)
      assert(!Money(0, USD).isPositive)

      assert(!Money(1, USD).isNegative)
      assert(!Money(1, USD).isZero)
      assert(Money(1, USD).isPositive)

      assert(Money(-1, USD).isNegative)
      assert(!Money(-1, USD).isZero)
      assert(!Money(-1, USD).isPositive)
    }
  }
}
