#!/bin/sh
java -XX:MaxPermSize=512M -XX:+CMSClassUnloadingEnabled -XX:+UseConcMarkSweepGC -Xmx1536M -Xss1M -jar `dirname $0`/sbt-launch.jar "$@"
