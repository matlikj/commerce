name := "commerce"

version := "0.1.0"

scalaVersion in ThisBuild := "2.10.4"

scalacOptions in ThisBuild ++= Seq("-deprecation", "-feature", "-unchecked")

val scalaMeterFramework = new TestFramework("org.scalameter.ScalaMeterFramework")

testFrameworks in ThisBuild += scalaMeterFramework

testOptions in ThisBuild += Tests.Argument(scalaMeterFramework, "-silent")

testOptions in ThisBuild += Tests.Argument(TestFrameworks.ScalaTest, "-oD")

logBuffered in ThisBuild := false

//parallelExecution in Test := false

concurrentRestrictions in Global += Tags.limit(Tags.Test, 1)

// enable improved (experimental) incremental compilation algorithm called "name hashing"
incOptions := incOptions.value.withNameHashing(true)

resolvers in ThisBuild ++= Seq(
  "Sonatype OSS Releases"  at "http://oss.sonatype.org/content/repositories/releases/",
  "Sonatype OSS Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/",
  "milestone-springframework" at "http://maven.springframework.org/milestone/"
)

// Look at json4s.org for a decent JSON library

// Libraries used in this build include:
//   Spring framework
//   Spring JDBC (for optional connection performance improvement LazyConnectionDataSourceProxy)
//   BoneCP JDBC Connection Pool -- http://jolbox.com/
//   Flyway JDBC schema migration mgmt (optional execution when opening database) -- http://flywaydb.org
//   H2 Relational Database (testing)
//   Akka Actor library -- http://akka.io

// Runtime dependencies
libraryDependencies in ThisBuild ++= List(
  "org.springframework.scala"     % "spring-scala_2.10"        % "1.0.0.RC1",
  //
  // Database libraries
  //"com.typesafe.slick"           %% "slick"                    % "2.0.0",
  //"com.jolbox"                    % "bonecp"                   % "0.8.0.RELEASE",
  //"com.jolbox"                    % "bonecp-spring"            % "0.8.0.RELEASE",
  //"org.springframework"           % "spring-jdbc"              % "3.2.4.RELEASE",
  //"com.h2database"                % "h2"                       % "1.3.175",
  //"com.googlecode.flyway"         % "flyway-core"              % "2.3",
  //
  // Akka for concurrency
  //"com.typesafe.akka"            %% "akka-actor"               % "2.2.3",
  //"com.typesafe.akka"            %% "akka-slf4j"               % "2.2.3",
  //"ch.qos.logback"                % "logback-classic"          % "1.0.0"        % "runtime",
  //
  "joda-time"                     % "joda-time"                % "2.3",
  "org.joda"                      % "joda-convert"             % "1.6",
  "org.scalactic"                %% "scalactic"                % "2.2.1",
  //
  "com.storm-enroute"            %% "scalameter"               % "0.6"
)

// Testing dependencies
libraryDependencies in ThisBuild ++= List(
  // Common libs useful for test cases
  "org.scalatest"                %% "scalatest"                % "2.2.1"        % "test",
  "junit"                         % "junit"                    % "4.5"          % "test",
  "org.scalacheck"               %% "scalacheck"               % "1.11.3"       % "test"
)

// These are some libs I've looked at using, but do not presently need
//  "com.chuusai" % "shapeless" % "2.0.0-M1" cross CrossVersion.full,
//  "org.scala-lang"               %% "scala-pickling" % "0.8.0-SNAPSHOT",

// =============================================================================
// Use the slickgen plugin under the "precompile" configuration to generate DAO 
// =============================================================================

lazy val Precompile = config("precompile")

lazy val modelgen = TaskKey[Seq[File]]("modelgen", "Generate/update model sources.")

lazy val modelgenClass = SettingKey[String]("The main class to run for generation")

lazy val modelgenOutDir = SettingKey[File]("Output dir for slickgen")

lazy val precompileSettings = inConfig(Precompile)(
  Defaults.defaultSettings ++ Defaults.configSettings ++ Defaults.configTasks)

lazy val compileEnhancement = inConfig(Compile)(Seq(
    modelgenOutDir <<= managedDirectory(_ / "modelgen"),
    modelgenClass := "matlik.slickgen.Modelgen",
    modelgen <<= runModelgen(Precompile, modelgenClass, sourceManaged),
    modelgen <<= modelgen dependsOn (compile in Precompile),
    //sourceGenerators in Compile <+= modelgen
    sourceGenerators in Compile <+= modelgen
  )
)

def runModelgen(config: Configuration, mainClassSetting: SettingKey[String], outDirSetting: SettingKey[File]): Def.Initialize[Task[Seq[File]]] =
  (fullClasspath in config, runner in (config, run), streams, mainClassSetting, outDirSetting) map { (cp, r, s, mainClass, outDir) =>
    if (!outDir.exists) { outDir.mkdirs }
    if (!outDir.exists) { error("Could not create managed source directory: " + outDir.getAbsolutePath) }
    toError(r.run(mainClass, Attributed.data(cp), Seq(outDir.getAbsolutePath), s.log))
    val finder: PathFinder = (outDir) ** "*.scala"
    finder.get
  }

lazy val slickgenSettings = (precompileSettings ++ compileEnhancement ++ Seq(EclipseKeys.createSrc := EclipseCreateSrc.Default + EclipseCreateSrc.Managed))


// =============================================================================
// Define nested projects
// =============================================================================

lazy val root = project.in( file(".") )
  .aggregate(core, measures, pricing)

lazy val core = project.in( file("core") )
  .dependsOn(measures)

lazy val measures = project.in( file("measures") )

lazy val pricing = project.in( file("pricing") )
  .dependsOn(core)

