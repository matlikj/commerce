# Pricing Module

My experience with several pricing solutions involved imperative, mutable
state, often executed within some kind of loop.  Even with those advertising
extensibility, extreme care is needed to avoid accidentally skewing price
calculations across each iteration, leading to strange results.  Under these
conditions, custom extensions become costly to develop well, and are fraught
with unanticipated edge cases.

This pricing library has been designed with immutable data structures and
functional (i.e. no side effects) business logic. The goal is to easily support
a plethora of pricing and promotional needs through composable objects.

The following features and design goals should be supported:

1. Pricing is calculated at the sales unit level, but should be easily
   aggregated to any level desired, such as entry or order level.

2. Support multiple basic price adjustments such as:
     - Simple per-unit fixed price.  This includes absolute prices, uplift and discounts.
     - Percentage uplift and discount.

3. Fine control over price application through filters (exclusion from calculation scope) and grouping.

4. Fixed price applications to groups of entries with configurable distribution (by quantity, price, and custom distribution ratios)

5. Allow fine control over stacking.  Avoid discounting too deeply by
   preventing overlapping promotions from compounding their discounts, but
   still support multiple discounts if needed.

6. Through combining the various price adjustments, filters, groupers, and
   price applications, support more complex pricing scenarios such as exclusive
   bundle pricing, buy x get y free or discounted, variant configuration
   surcharges, free or discounted shipping, scale based pricing, etc.

7. Provide a trail of calcualtion results to explain how prices were
   determined.  Post processing analysis of the calculation result log should
   be all that is needed when verifying and understanding unexpected results after
   calculation.  No deep dive debug sessions should be required, and ideally the
   result log will be understandable enough for business users to digest.

8. Support messaging at every step, usable both for debug/support (e.g. could
   not apply discount because price row was not found) as well as fedback to
   end users (e.g. explain a promotion did not apply because the product is in an
   exclusion list)

9. Support real-time price simulation.  This should not require a fully
   configured sales document, and should be executable for a single product (at
   least).

## Customizable

There is more to being customizable than being open sourced.

First, the solution tries not to make too many assumptions about the
application for which it is embedded. The core functionality bases its
calculations on the entry, its unit quantity and unit prices loaded through
custom extensions. Defining the unit of measure and assigning quantity to an
entry is accomplished through the creation of a CalculationContext, which is
the responsibility of the calling application code.  This CalculationContext
serves as an extensible envelope to contain both standard fields (defined
within CalculationContext trait) as well as custom (via concrete
implementations) data for use throughout the pricing logic. This envelope
enables the calling logic to easily pass domain specific data to other custom
extensions deeper in the pricing calculation. The same approach has been taken
to support messaging; however, Messages are bound to the result objects
allowing domain specific data to flow from extension points back up to the
caller.

Second, the business logic has been designed with an eye on composability and
reuse. At a high level, the structure of the pricing solution is as follows:

1. A CalculationContext serves as an envelope for common inbound data into
   a pricing calculation, and is propagated throughout. Within are pre-defined
   attributes useful for identifying where in the pricing calculation the current
   logic resides, the results of previous calculation steps (a partially completed
   PriceDocument), intermediate results of the current calculation step (list of
   entries in scope via the stepView), and the like.  The CalculationContext has
   also been defined as a trait (translates to a Java Interface) so it can be
   implemented with a concrete class containing other custom attributes needed to
   support extension logic.

2. A PriceDocument is ultimately the end result of a price calculation, which
   is composed of many CalculationStepResult objects.

3. A PriceCalculator provides the top level API to the calling the calculation
   business logic, accepting a CalculationContext (inbound data object) and
   returning a PriceDocument (outbound data object). The PriceCalculator is
   composed of a sequence of CalculationValueStep objects to be processed in
   order. The results of previously processed CalculationValueSteps are collected
   into an intermediate PriceDocument that is stored in the CalculationContext
   sent to the next CalculationValueStep to be processed.

4. A CalculationValueStep is a named (e.g. "base price", "taxes", etc)
   CalculationStep that may calculate prices stand-alone or based off
   previously calculated values (stackable) in the PriceDocument as needed.

5. A CalculationStep accepts a CalculationContext and returns a collection of
   CalculationStepResult. This type is where most of the configurability and
   extensibility lives.
     - It primarily processes the CalculationContext's stepView, a collection of
       potentially relevant entry prices to be mutated via the immutable result.
     - The impact of a CalculationStep on the stepView may be applied to the
       CalculationContext before calling another CalculationStep recursively
     - A composable version supports combining implementations of
       StepRestriction, EntryPriceRestriction, Grouper, PriceRowSelector and
       ResultCalculator to support complex pricing requirements relatively easily.
     - By default, CalculationSteps are designed not to stack (a sensible
       default), but this can be overridden.  A common case is when the
       CalculationStep serves as a CalculationValueStep.

Third, if functionality you need does not exist, it should be fairly easy to
write your own. To introduce custom business logic, simply extend existing
classes or implement to interface (traits) and compose as you would normally.

## Traceable

The data model is immutable and roughly falls into two categories: what is (at
a given moment), and changes made.

The predominant type falling into "what is" is the EntryPrice, which describes
how much quantity receives a given per price for an entry.  A single entry
(identified by an EntryId) may have multiple EntryPrices.  For example, an
entry identified by EntryID "1" may have a total quantity of 3 split between
two EntryPrices.  The first EntryPrice has quantity 2 at $4 while the second
EntryPrice has quantity 1 at $2.

Understanding why the quantity was priced differently can be explained by
looking at the "changes made" via the CalculationStepResults.  There are
several CalculationStepResult subtypes that explain what action (if any) each
processed CalculationStep takes on the inbound EntryPrices, such as discounting
a subset of an entry's quantity by 50%.

When the CalculationStep's before and after price impact described by the
CalculationStepResult is not enough, it can also provide Messages for a more
detailed explanation. Such messages may be generated for internal use only, or
they may be used to facilitate clarification or up-sell messages to the
customer.  One such up-sell message may look like "Purchase one more shirt and
save 50%" while a clarification may look like "This book title is excluded from
promotional discounts".

The PriceDocument records the full provenance of the pricing calculation
including the collection of EntryPrices and CalculationStepResults for each
CalculationValueStep and the CalculationSteps within.

# Example Composition Recipes

Please see the PriceCalculatorExamples class used for unit testing for working
examples of how components may be composed. The examples provided are far from
comprehensive (if such a list could exist) but should provide a reasonable
overview of the minimal amount of work needed.

TODO: Document a list of approaches to address common use cases.

# Possible Future Enhancements
Some design goals that are under consideration but have not yet been
implemented include:

1. Phased price calculation support to reduces the need for full price document
   recalculation as more information is provided.  Partially calculated price
   documents can be restored (possibly in part) when continuing to complete the
   whole document's calculation.  The idea is to not require a full recalculation,
   but instead only calculate that which is needed.  For example, there is no need
   to redetermine base price and standard promotions once delivery information is
   provided to calculate tax and shipping.  Just calculate the tax and shipping
   using the previously determined values.

2. Ahead of time price simulation over a time duration.  This is useful for
   providing prices to a search index, thus allowing the indexed values to be
   post-processed in an extremely light weight way.  While the details of how this
   works has yet to be determined, a solution like the following has been useful
   in the past.  First all possible prices are calculated for a given date range
   and serialized into a format that can be stored and returned by search results.
   This assumes all price rows impacting the price are known ahead of time, and
   more complex (i.e. spanning more quantity than 1) price adjustments are not
   supported.

        base: START ----------------------------------------END TIME
        price1:           0------0
        price2:                          0-------------0
        price3:                0-------------------0
        price4:                     0---------0
        actual: --base----|-p1-|-p3-|---p4----|-p3-|-p2-|-base--
   This diagram shows greatest priority at the bottom.  Upon receiving this
   information from a search query, the one price must be selected from the
   collection.  There are two pieces of information needed when selecting the
   appropriate price in this approach: priority and current time.  First all
   possible prices in the given date range should be filtered by the validity
   date, and then the one record with the highest priority should be selected as
   the winner.  This does require some processing of the data stored in the search
   results, so it doesn't work well for directly dumping the search results to the
   screen, but it does eliminate a significant amount of overhead if a real-time
   simulation is to be performed with every view of an item, even if caching is
   involved (first user receives the performance penalty).  Note, however, this
   approach also requires that prices are planned ahead of time.  Otherwise a full
   re-index is required to bring the search index back up to speed.

3. Scale based price simulation.  At a given point in time, you don't calculate
   a unit price assuming the quantity is 1, but instead calculate the unit
   price for a variety of quantities since the unit price may decrease with the
   greater number of products.  This is often driven by base price, but may also
   be promotional in nature.  A generic solution would be ideal.

4. Promotion codes.  Promotion activation should be considered only when
   a promotion code is active.  Potentially relevant promotions should be
   constrained by activation date, but promotion code could be used as well.

5. One Time Use Promotion Codes.
   *Design Recommendation Outside Pricing Module Required*  A user entered
   promotion code could be entered on the cart and redeemed only once.  Upon being
   redeemed, it should no longer be valid/usable.  This may best be implemented as
   a bolt on to the cart that abstracts on top of the promotion code activation.

6. Add a Java API to support easy integration with Java only codebases.

7. Sample implementations and recipes for common price scenarios

8. Implement a PriceCalculator that can configure the CalculationValueStepSeq
   using JSON or XML, possibly through case class deserialization.

9. Create a PriceDocument viewer that can render a graph for easily visualizing
   how CalculationStepResults impact EntryPrices.  This could possibly use
   a JavaScript library for use within a browser.
