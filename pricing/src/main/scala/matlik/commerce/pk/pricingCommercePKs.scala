package matlik.commerce.pk

import matlik.core.PK

trait EntryPricePK extends PK
trait CalculationStepPK extends PK