package matlik.commerce.pricing

/**
 * The CalculationValue stores the results of a CalculationStep in conjunction with the produced EntryPrice values.
 *
 * @param name a unique identifier within the context of a PriceDocument containing the CalculationValue. For example,
 * there should be one CalculationValue for the base price, and that CalculationValue will contain all results and
 * prices for all entries in scope for the price calculation.
 * @param results the 
 */
case class CalculationValue(
  name: CalculationValueName,
  results: Seq[CalculationResult],
  prices: Seq[EntryPrice])

case class CalculationValueName(name: String)