package matlik.commerce.pricing

import matlik.common.Message

/** Determines if a calculation step is applicable. */
trait CalculationStepRestrictor {
  def getStepRestriction(context: CalculationContext): StepRestriction
}

object CalculationStepRestrictor {
  def allow(context: CalculationContext, messages: Seq[Message] = Seq.empty) = StepAllowed(step = context.calculationStep, depth = context.calculationDepth, messages = messages)
  def deny(context: CalculationContext, messages: Seq[Message] = Seq.empty) = StepDenied(step = context.calculationStep, depth = context.calculationDepth, messages = messages)
}
