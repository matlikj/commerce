package matlik.commerce.pricing




/*
 * TODO: Determine if the following objects are actually needed.  
 * 1. The result objects serve as the persisted objects for the cart/order
 * 2. Some other mechanism is needed to drive the configuration.  The below is Promotion specific, but applicable
 */

///** 
// * A single instance of a promotion firing. If it applies, the cart/order was impacted by the promotion.  If
// * not, the promotion engine tried to fire, but some requirement wasn't fully met.  In this case, an instance
// * that does not apply could be used to trigger up-sell text to the customer
// */
//trait TriggeredPromotion extends Identified[TriggeredPromotionID] {
//  def promotion: Option[PromotionID]
//  def applied: Boolean
//}
//case class TriggeredPromotionID(value: Long) extends PKLong
//
//object PromotionGroup {
//  abstract class ActivationType
//  case object CART
//  case object PRODUCT
//  case object PROMOCODE
//}

///**
// * Contains the master data needed to define activation scope of a collection of promotions.
// */
//trait PromotionGroup extends Identified[PromotionGroupID] {
//  import PromotionGroup._
//  def promovalue: String
//  def activationType: ActivationType
//}
//case class PromotionGroupID(value: String) extends PKString

///**
// * Contains the master data needed to configure/drive the promotion within the promotion engine.
// */
//trait PromotionDetails extends Identified[PromotionID] {
//  def group: PromotionGroupID
//  def description: String
//}
//case class PromotionID(value: Long) extends PKLong
