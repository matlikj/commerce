package matlik.commerce.pricing

import matlik.commerce.pricing.CalculationStep._
import matlik.measures.Converter
import matlik.measures.Currency
import matlik.measures.Money
import matlik.commerce.pk.CalculationStepPK

abstract class CalculationStepSequence extends CalculationStep with CalculationStepRestrictor {
  def steps: Seq[CalculationStep]

  def enrichContext(context: CalculationContext) = CalculationStep.commonContextEnrichment(id, context)
  
  /** Allow the step by default */
  def getStepRestriction(context: CalculationContext): StepRestriction = CalculationStepRestrictor.allow(context)

  override def calculate(contextIn: CalculationContext)(implicit converter: Converter[Money, Currency]): Seq[CalculationStepResult] = {
    val context = enrichContext(contextIn)

    getStepRestriction(context) match {
      case allowed: StepAllowed => {

        val (endContext, endResults) = steps.foldLeft((context, Seq.empty[CalculationStepResult])) { (carry, step) =>
          val (ctx, results) = carry
          if (ctx.stepView.isEmpty) carry
          else {
            val calculatedResults = step.calculate(ctx)
            val (modifiedEntries, remainingEntries) = filterEntriesByCalculationResults(ctx.stepView, calculatedResults)
            (ctx.copyContext(stepView = remainingEntries), results ++ calculatedResults)
          }
        }

        val applies = endResults.exists(r => r.isInstanceOf[ApplicationResult] && r.asInstanceOf[ApplicationResult].applies)
        Seq(allowed, CompoundApplication(
          step = context.calculationStep,
          depth = context.calculationDepth,
          childResults = endResults,
          applies = applies))
      }
      case denied: StepDenied => Seq(denied)
    }
  }
}

case class ComposibleCalculationStepSequence(val id: CalculationStepPK, val steps: Seq[CalculationStep], stepRestrictor: Option[CalculationStepRestrictor] = None) extends CalculationStepSequence {
  val stepRestrictionF = stepRestrictor.map(_.getStepRestriction _).getOrElse(super.getStepRestriction _)
  override def getStepRestriction(context: CalculationContext) = stepRestrictionF(context)
}