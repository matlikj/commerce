package matlik.commerce.pricing

import matlik.core.Identified
import matlik.core.PKLong
import matlik.core.PKString
import matlik.measures.Money
import matlik.commerce.Quantity
import matlik.common.ValidityDates
import matlik.commerce.pk._
import org.joda.time.DateTime


/**
 * An extensible envelope for containing relevant information needed for efficient pricing logic.  While there are a few required features to
 * support standard functionality, the intent is for concrete implementations of this trait to contain custom information needed to support
 * extension points throughout the pricing framework.  This could include, for example: 
 * - Pricing history across price steps
 * - Pricing history within the current price step
 * - Access to the untouched order's base pricing of all entries
 * - Restricted view of entries due to quantity consumption (to prevent stacking of promotions)
 *
 * Ultimately, this functionality should accept EntryPrices and return EntryPrices in a lazy and parallel way.
 */
trait CalculationContext {
  def priceDocument: Option[PriceDocument]
  def stepView: Seq[EntryPrice]
  def calculationStep: Option[CalculationStepPK]
  def calculationDepth: Option[Int]
  def copyContext(
      stepView: Seq[EntryPrice] = stepView, 
      priceDocument: Option[PriceDocument] = priceDocument,
      calculationStep: Option[CalculationStepPK] = calculationStep,
      calculationDepth: Option[Int] = calculationDepth): CalculationContext
}