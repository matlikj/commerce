package matlik.commerce.pricing

import matlik.commerce.pricing.CalculationStep._

/**
 * Select one price row for each ApplicationResultGroup.  The CalculationView provides context for selection
 *
 * Input: Only groups to apply prices to (filtered out "could have fired" groups from Grouper results)
 * Output: A 1:1 list of PriceRows
 */
trait PriceRowSelector {
  def selectPriceRows(context: CalculationContext, groups: Seq[ApplicationResultGroup]): Seq[PriceRow]
}

class BasicPriceRowSelector extends PriceRowSelector {
  override def selectPriceRows(context: CalculationContext, groups: Seq[ApplicationResultGroup]): Seq[PriceRow] = ???
}