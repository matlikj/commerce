package matlik.commerce.pricing

import matlik.commerce.Quantity
import matlik.common._
import matlik.measures._
import org.scalactic.ConversionCheckedTripleEquals._

/**
 *  Given a list of incomplete result groups, and their corresponding price rows, apply the prices with
 *  a specified algorithm (fixed, even distribution, amount to apply, rounding rules).
 */
trait ResultCalculator {
  def calculateResults(context: CalculationContext, groups: Seq[ApplicationResultGroup], priceRows: Seq[PriceRow])(implicit converter: Converter[Money, Currency]): Seq[CalculationStepResult]
}

abstract class AbstractResultCalculator extends ResultCalculator {
  override def calculateResults(context: CalculationContext, groups: Seq[ApplicationResultGroup], priceRows: Seq[PriceRow])(implicit converter: Converter[Money, Currency]): Seq[CalculationStepResult] = {
    assert(groups.size === priceRows.size, 
        s"""|There should be an equal number of groups as priceRows provide.  groups=${groups.size}, priceRows=${priceRows.size}
            |  groups: ${groups}
            |  priceRows: ${priceRows}""".stripMargin)
    val pairs = groups zip priceRows
    return doCalculateResults(context, pairs);
  }

  def updateResult(ea: EntryApplication, row: PriceRow, price: Option[Money]) = {
    val messages = row match {
      case r: Messages => ea.messages ++ r.messages
      case _ => ea.messages
    }
    ea.copy(
      startDate = ValidityDates.later(row.startDate, ea.startDate),
      endDate = ValidityDates.earlier(row.endDate, ea.endDate),
      unitPrice = price,
      applies = true,
      messages = messages)
  }

  def doCalculateResults(context: CalculationContext, pairs: Seq[(ApplicationResultGroup, PriceRow)])(implicit converter: Converter[Money, Currency]): Seq[CalculationStepResult]
}

abstract class AbstractPercentResultCalculator extends AbstractResultCalculator {
  override def doCalculateResults(context: CalculationContext, pairs: Seq[(ApplicationResultGroup, PriceRow)])(implicit converter: Converter[Money, Currency]): Seq[CalculationStepResult] = {
    for {
      (group, priceRow) <- pairs
      percentPriceRow = priceRow.asInstanceOf[PercentPriceRow]
      result <- group
    } yield {
      result match {
        case ea: EntryApplication => ea.unitPrice match {
          case None => ea
          case Some(unitPrice) => updateResult(ea, percentPriceRow, Option(calculatePrice(context, result, percentPriceRow, unitPrice)))
        }
      }
    }
  }

  def calculatePrice(context: CalculationContext, result: ApplicationResult, priceRow: PercentPriceRow, unitPrice: Money)(implicit converter: Converter[Money, Currency]): Money
}

/** Increase the original price by the specified percent */
object PercentIncreaseResultCalculator extends AbstractPercentResultCalculator {
  override def calculatePrice(context: CalculationContext, result: ApplicationResult, priceRow: PercentPriceRow, unitPrice: Money)(implicit converter: Converter[Money, Currency]): Money = {
    val priceAdjustment = unitPrice * priceRow.percent / 100
    val adjustedPrice = unitPrice + priceAdjustment
    adjustedPrice.round()
  }
}

/** Decrease the original price by the specified percent */
object PercentDecreaseResultCalculator extends AbstractPercentResultCalculator {
  override def calculatePrice(context: CalculationContext, result: ApplicationResult, priceRow: PercentPriceRow, unitPrice: Money)(implicit converter: Converter[Money, Currency]): Money = {
    val priceAdjustment = unitPrice * priceRow.percent / 100
    val adjustedPrice = unitPrice - priceAdjustment
    adjustedPrice.round()
  }
}

/** Set the price to be the specified percent of the original price */
object PercentPriceResultCalculator extends AbstractPercentResultCalculator {
  override def calculatePrice(context: CalculationContext, result: ApplicationResult, priceRow: PercentPriceRow, unitPrice: Money)(implicit converter: Converter[Money, Currency]): Money = {
    val adjustedPrice = unitPrice * priceRow.percent / 100
    adjustedPrice.round()
  }
}

/**
 * Apply a fixed unit price to each result in the result group.  It is possible to have more than one fixed
 * price apply by providing more than one application result group each with a corresponding price row. Note
 * that only EntryApplication results will have their price set while other result types are left untouched.
 */
object FixedUnitPriceResultCalculator extends AbstractResultCalculator {
  override def doCalculateResults(context: CalculationContext, pairs: Seq[(ApplicationResultGroup, PriceRow)])(implicit converter: Converter[Money, Currency]): Seq[CalculationStepResult] = {
    for {
      (group, priceRow) <- pairs
      fixedPriceRow = priceRow.asInstanceOf[FixedPriceRow]
      result <- group
    } yield {
      result match {
        case ea: EntryApplication => updateResult(ea, fixedPriceRow, Option(fixedPriceRow.price))
      }
    }
  }
}

abstract class AbstractDistributedFixedPriceResultCalculator extends AbstractResultCalculator with ShareDistribution with FixedUnitPriceCalculator {
  def getSharesOfDistribution(context: CalculationContext, group: ApplicationResultGroup): Seq[Double]

  /**
   * Converts any arbitrary share of a price into a value evenly divisible by the given quantity since
   * it is not possible to charge a customer a fraction of a cent. The returned value should be close to the
   * the shareValue * share, but evenly divisible by the quantity given the Money rounding rules.
   */
  def determineTotalShareForEntry(quantity: Quantity, shareValue: Money, share: Double)(implicit converter: Converter[Money, Currency]): Money = {
    val entrySharePrice = shareValue * share
    val unitSharePrice = entrySharePrice / quantity.qty
    unitSharePrice.round() * quantity.qty
  }

  def calculateUnitPrice(context: CalculationContext, result: ApplicationResult, priceRow: FixedPriceRow, unitPriceAdjustment: Money)(implicit converter: Converter[Money, Currency]): Money

  override def doCalculateResults(context: CalculationContext, pairs: Seq[(ApplicationResultGroup, PriceRow)])(implicit converter: Converter[Money, Currency]): Seq[CalculationStepResult] = {
    val result = for { (group, priceRow) <- pairs } yield {
      val fixedPriceRow = priceRow.asInstanceOf[FixedPriceRow]
      val shares = getSharesOfDistribution(context, group)
      val totalShares = shares.sum
      var remaining = fixedPriceRow.price
      val shareValue = remaining / totalShares
      var lastResult: Option[(EntryApplication, EntryApplication, Money)] = null

      // Calculate the application to the group
      val groupApplication: ApplicationResultGroup = for { (result, share) <- group zip shares } yield {
        result match {
          case ea: EntryApplication => {
            val adjustment = determineTotalShareForEntry(ea.quantity, shareValue, share)
            remaining = remaining - adjustment
            val unitAdjustment = adjustment / ea.quantity.qty
            val unitPrice = calculateUnitPrice(context, result, fixedPriceRow, unitAdjustment)
            val updated = updateResult(ea, fixedPriceRow, Option(unitPrice))
            // Store the before and after in case the remainder must be applied
            if (unitAdjustment.isPositive) {
              lastResult = Option((ea, updated, unitAdjustment))
            }
            updated
          }
        }
      }

      // Handle rounding error if the remaining value is non-zero.  Apply to the last entry in the Seq
      if (remaining.qty !== Money.ZERO) {
        val (before, origAfter, origAdjustment) = lastResult.get
        val split: Seq[EntryApplication] = {
          /* Two results may happen here.
           * 1. The last touched result has a qty 1 and needs to be adjusted by the remainder
           * 2. The last touched result has a qty > 1 and needs to be split into a new result with price adjusted remainder
           */
          val remainderAdjustment = origAdjustment + remaining
          val remainderUnitPrice = calculateUnitPrice(context, before, fixedPriceRow, remainderAdjustment)
          if (origAfter.quantity.qty === 1) {
            Seq(updateResult(before, fixedPriceRow, Option(remainderUnitPrice)))
          } else {
            val newAfter = origAfter.copy(quantity = origAfter.quantity - 1)
            val newBefore = before.copy(quantity = Quantity(1, origAfter.quantity.unit), toEntryPrice = new EntryPriceID())
            val remainderResult = updateResult(newBefore, fixedPriceRow, Option(remainderUnitPrice))
            Seq(newAfter, remainderResult)
          }
        }

        val idx = groupApplication.lastIndexWhere(_ === origAfter)
        val (left, right) = groupApplication.splitAt(idx)
        val orig = right.head

        left ++ split ++ right.tail
      } else groupApplication
    }
    result.flatten
  }
}

class ComposibleDistributedFixedPriceResultCalculator(shareDistributor: ShareDistribution, fixedUnitPriceCalculator: FixedUnitPriceCalculator) extends AbstractDistributedFixedPriceResultCalculator {
  override def getSharesOfDistribution(context: CalculationContext, group: ApplicationResultGroup): Seq[Double] = shareDistributor.getSharesOfDistribution(context, group)
  override def calculateUnitPrice(context: CalculationContext, result: ApplicationResult, priceRow: FixedPriceRow, unitPriceAdjustment: Money)(implicit converter: Converter[Money, Currency]): Money =
    fixedUnitPriceCalculator.calculateUnitPrice(context, result, priceRow, unitPriceAdjustment)
}

trait ShareDistribution {
  /**
   * Specify how the fixed price should be distributed.  The distribution will be applied by using a given
   * entry price's value as the numerator, and the total of all entry price values as the denominator. This
   * allows for simple return values, such as returning a map from group to quantity for a quantity based distribution.
   */
  def getSharesOfDistribution(context: CalculationContext, group: ApplicationResultGroup): Seq[Double]
}

object QuantityBasedShareDistribution extends QuantityBasedShareDistribution
trait QuantityBasedShareDistribution extends ShareDistribution {
  override def getSharesOfDistribution(context: CalculationContext, group: ApplicationResultGroup): Seq[Double] = {
    for (e <- group) yield {
      e.asInstanceOf[EntryApplication].quantity.qty.toDouble
    }
  }
}

object PriceBasedShareDistribution extends PriceBasedShareDistribution
trait PriceBasedShareDistribution extends ShareDistribution {
  override def getSharesOfDistribution(context: CalculationContext, group: ApplicationResultGroup): Seq[Double] = {
    for (e <- group) yield {
      val entryPrice = e.asInstanceOf[EntryApplication]
      val qty = entryPrice.quantity.qty.toDouble
      val unitPrice = entryPrice.unitPrice.map(_.qty.toDouble).getOrElse(0.0)
      unitPrice * qty
    }
  }
}

trait FixedUnitPriceCalculator {
  def calculateUnitPrice(context: CalculationContext, result: ApplicationResult, priceRow: FixedPriceRow, unitPriceAdjustment: Money)(implicit converter: Converter[Money, Currency]): Money
}

object FixedUnitPrice extends FixedUnitPrice
trait FixedUnitPrice extends FixedUnitPriceCalculator {
  override def calculateUnitPrice(context: CalculationContext, result: ApplicationResult, priceRow: FixedPriceRow, unitPriceAdjustment: Money)(implicit converter: Converter[Money, Currency]): Money = {
    assert(unitPriceAdjustment.qty >= Money.ZERO)
    unitPriceAdjustment.round()
  }
}

object FixedUnitPriceUplift extends FixedUnitPriceUplift
trait FixedUnitPriceUplift extends FixedUnitPriceCalculator {
  override def calculateUnitPrice(context: CalculationContext, result: ApplicationResult, priceRow: FixedPriceRow, unitPriceAdjustment: Money)(implicit converter: Converter[Money, Currency]): Money = {
    val potential = (result.asInstanceOf[EntryApplication].unitPrice.get + unitPriceAdjustment).round()
    if (potential.qty < Money.ZERO) Money(Money.ZERO, potential.cur) else potential
  }
}

object FixedUnitPriceDiscount extends FixedUnitPriceDiscount
trait FixedUnitPriceDiscount extends FixedUnitPriceCalculator {
  override def calculateUnitPrice(context: CalculationContext, result: ApplicationResult, priceRow: FixedPriceRow, unitPriceAdjustment: Money)(implicit converter: Converter[Money, Currency]): Money = {
    val potential = (result.asInstanceOf[EntryApplication].unitPrice.get - unitPriceAdjustment).round()
    if (potential.qty < Money.ZERO) Money(Money.ZERO, potential.cur) else potential
  }
}
