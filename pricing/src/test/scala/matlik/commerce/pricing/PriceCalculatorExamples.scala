package matlik.commerce.pricing

import scala.collection.mutable
import matlik.commerce.pk.EntryPK
import matlik.measures._
import matlik.measures.SimpleImplicits._
import CalculationStep._
import PriceCalculator._
import scala.annotation.tailrec

object PriceCalculatorExamples {

  /* Used to choose how much of the CalculationValueStep sequence to use for a given price run in automated test cases */
  case class RestrictByTestLevel(level: Int) extends CalculationStepRestrictor {
    import CalculationStepRestrictor._
    def getStepRestriction(context: CalculationContext): StepRestriction = {
      context match {
        case c: SimpleCalculationContext => if (c.testLevel >= level) allow(context) else deny(context)
        case _ => CalculationStepRestrictor.deny(context)
      }
    }
  }

  /* Decide if an entry should be restricted from a PriceCalculation if the predicate function returns true */
  case class EntryIdRestrictor(predicate: (EntryID) => Boolean) extends EntryPriceRestrictor {
    override def getEntryPriceRestrictions(context: CalculationContext): Seq[EntryPriceRestriction] = {
      import EntryPriceRestrictor._
      context.stepView.map { e =>
        if (predicate(e.entry.asInstanceOf[EntryID])) {
          allow(context, e)
        } else {
          deny(context, e)
        }
      }
    }
  }

  /* A PriceRowSelector that returns a SimplePercentPriceRow with the specified percent for each group */
  case class PercentPriceRowSelector(percent: Double) extends PriceRowSelector {
    val priceRow = SimplePercentPriceRow(percent)
    override def selectPriceRows(context: CalculationContext, groups: Seq[ApplicationResultGroup]): Seq[PriceRow] = {
      for {
        group <- groups
      } yield {
        priceRow
      }
    }
  }
  

  
  //
  // Base Price price step
  // Applies to all entries -- TODO: Error scenario: What if a price cannot be found
  //

  /** Derives a fixed price row using the EntryID's value unless a price row is explicitly set in the map. */
  object BasePriceRowSelector extends PriceRowSelector {
    val priceRows: mutable.Map[EntryPK, PriceRow] = mutable.Map.empty
    override def selectPriceRows(context: CalculationContext, groups: Seq[ApplicationResultGroup]): Seq[PriceRow] = {
      for {
        group <- groups
        result <- group if result.isInstanceOf[EntryApplication]
        r = result.asInstanceOf[EntryApplication]
        entryID = r.entry.asInstanceOf[EntryID]
      } yield {
        priceRows.getOrElse(entryID, SimpleFixedPriceRow(Money(entryID.value, USD)))
      }
    }
  }

  object BasePriceStep extends ComposibleCalculationStep(
    stepID = CalculationStepID(0),
    grouper = BatchEntryGrouper,
    priceRowSelector = BasePriceRowSelector,
    resultCalculator = FixedUnitPriceResultCalculator,
    stepRestrictor = None,
    entryRestrictor = None)

  //
  // Percentage discount (part of "Discounted" calculation value)
  // Applies a 10% discount to all odd numbered entry IDs
  //

  object PercentDiscountStep extends ComposibleCalculationStep(
    stepID = CalculationStepID(2),
    grouper = BatchEntryGrouper,
    priceRowSelector = PercentPriceRowSelector(10),
    resultCalculator = PercentDecreaseResultCalculator,
    stepRestrictor = Option(RestrictByTestLevel(2)),
    entryRestrictor = Option(EntryIdRestrictor(e => e.value % 2 == 1)))

  //
  // Bundle discount (part of "Discounted" calculation value)
  // Applies a 50% discount to 3 different entries whose IDs are divisible by 5 only if all 3 are priced together
  //
  
  val BundleDiscountStepSuccessMessage = SimpleMessage("50% bundle promotion applied")
  val BundleDiscountStepFailMessage = SimpleMessage("50% bundle could have applied, but missing product")
  object BundleDiscountStep extends ComposibleCalculationStep(
    stepID = CalculationStepID(3),
    grouper = OnePerEntryGrouper(
        numOfEntries = Option(3), 
        successMessages = Seq(BundleDiscountStepSuccessMessage), 
        failMessages = Seq(BundleDiscountStepFailMessage)),
    priceRowSelector = PercentPriceRowSelector(50),
    resultCalculator = PercentDecreaseResultCalculator,
    stepRestrictor = Option(RestrictByTestLevel(3)),
    entryRestrictor = Option(EntryIdRestrictor(e => e.value % 5 == 0)))

  //
  // Compose the root of the "Discounted" calculation value.
  // CalculationSteps earlier in the sequence take precedence over subsequent CalculationSteps
  // If an earlier CalculationStep applies to quantity, that quantity will be "consumed".
  // The unconsumed quantity will be passed to the subsequent steps for processing.
  //
  object DiscountStep extends ComposibleCalculationStepSequence(CalculationStepID(1), Seq(BundleDiscountStep, PercentDiscountStep))

  //
  // Shipping & Handling
  // Applies a 2 USD charge to all but IDs divisible by 15
  //
  
  val shippingFixedPriceRowSelector = new PriceRowSelector() {
    override def selectPriceRows(context: CalculationContext, groups: Seq[ApplicationResultGroup]): Seq[PriceRow] = {
      for {
        group <- groups
      } yield {
        // Obviously a hack for testing :)
        val e = group.head.asInstanceOf[EntryApplication].entry.asInstanceOf[EntryID]
        if (e.value % 15 == 0) SimpleFixedPriceRow(Money(0, USD))
        else SimpleFixedPriceRow(Money(2, USD))
      }
    }
  }
  
  object ShippingStep extends ComposibleCalculationStep(
    stepID = CalculationStepID(10),
    grouper = BatchEntryGrouper,
    priceRowSelector = shippingFixedPriceRowSelector,
    resultCalculator = FixedUnitPriceResultCalculator,
    stepRestrictor = Option(RestrictByTestLevel(10)),
    entryRestrictor = None)

  //
  // Subtotal
  // Given the discount and the shipping, create prices to contain their sum
  //
  
  object SubtotalStep extends CalculationValueSummingCalculationStep(
      id = CalculationStepID(11), 
      valueNames = Seq(CV_DISCOUNTED, CV_SHIPPING),
      mergeCriteria = MergeCriteriaByRealtiveEntryPriceOrdering(GreaterUnitPriceOrdering))
  
  //
  // Taxes
  // Applies a 10% charge to the total of discounted and shipping
  //
  
  val taxPriceRowSelector = new PriceRowSelector() {
    val taxRate = SimplePercentPriceRow(10)
    override def selectPriceRows(context: CalculationContext, groups: Seq[ApplicationResultGroup]): Seq[PriceRow] = {
      for {
        group <- groups
      } yield {
        taxRate
      }
    }
  }
    
  object TaxStep extends ComposibleCalculationStep(
    stepID = CalculationStepID(12),
    grouper = AllGrouper,
    priceRowSelector = taxPriceRowSelector,
    resultCalculator = PercentPriceResultCalculator,
    stepRestrictor = Option(RestrictByTestLevel(12)),
    entryRestrictor = None)
  
  //
  // Gross Price
  // The total price a customer is expected to pay
  //
  
  object GrossStep extends CalculationValueSummingCalculationStep(
      id = CalculationStepID(13), 
      valueNames = Seq(CV_SUBTOTAL, CV_TAX),
      mergeCriteria = MergeCriteriaByRealtiveEntryPriceOrdering(GreaterUnitPriceOrdering))
  
  //
  // Configure the pricer
  //

  object CV_BASE_PRICE extends CalculationValueName("Base Price")
  object CV_DISCOUNTED extends CalculationValueName("Discounted")
  object CV_SHIPPING extends CalculationValueName("Shipping")
  object CV_SUBTOTAL extends CalculationValueName("Subtotal")
  object CV_TAX extends CalculationValueName("Taxes")
  object CV_GROSS extends CalculationValueName("Gross")

  val calculationValueSequenceLoader = new CalculationValueSequenceLoader {
    val cvSteps = Seq(
    CalculationValueStep(CV_BASE_PRICE, BasePriceStep),
    CalculationValueStep(CV_DISCOUNTED, DiscountStep),
    CalculationValueStep(CV_SHIPPING, ShippingStep),
    CalculationValueStep(CV_SUBTOTAL, SubtotalStep),
    CalculationValueStep(CV_TAX, TaxStep),
    CalculationValueStep(CV_GROSS, GrossStep))
      
    def loadCalculationValueSequence(context: CalculationContext): Seq[CalculationValueStep] = {
      cvSteps.filter(cvStep => cvStep.step.id.asInstanceOf[CalculationStepID].value <= context.asInstanceOf[SimpleCalculationContext].testLevel)
    }
  }

  val defaultPriceDocumentLoader = new DefaultPriceDocumentLoader {
    def loadDefaultPriceDocument(context: CalculationContext): PriceDocument = SimplePriceDocument()
  }

  val pricer = new ComposiblePriceCalculator(calculationValueSequenceLoader, defaultPriceDocumentLoader)
}
