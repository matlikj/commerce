package matlik.commerce.pricing

import org.joda.time.DateTime
import org.junit.runner.RunWith
import org.scalatest.WordSpec
import org.scalatest.Matchers
import matlik.measures._
import matlik.measures.SimpleImplicits._
import matlik.commerce.Quantity
import matlik.commerce.Quantity._
import matlik.commerce.pk._
import matlik.common.Message

@RunWith(classOf[org.scalatest.junit.JUnitRunner])
class PercentResultCalculatorSpec extends WordSpec with Matchers {

  import matlik.measures.SimpleImplicits._

  val context = SimpleCalculationContext( stepView = Seq.empty[EntryPrice])

  val ep1 = SimpleEntryPrice(id = Option(new EntryPriceID()), entry = EntryID(1), quantity = Quantity(1, EACH), unitPrice = Option(Money(10, USD)))
  val ep2 = SimpleEntryPrice(id = Option(new EntryPriceID()), entry = EntryID(2), quantity = Quantity(2, EACH), unitPrice = Option(Money(20, USD)))
  val ep3 = SimpleEntryPrice(id = Option(new EntryPriceID()), entry = EntryID(3), quantity = Quantity(3, EACH), unitPrice = Option(Money(30, USD)))

  "AbstractPercentResultCalculator" should {
    val price = Money(100, USD)
          
    val dummy = new AbstractPercentResultCalculator() {
      override def calculatePrice(context: CalculationContext, result: ApplicationResult, priceRow: PercentPriceRow, unitPrice: Money)(implicit converter: Converter[Money, Currency]): Money = {
        price
      }
    }
      
    "audit for preconditions" in {
      val pr = SimplePercentPriceRow(10)
      val ea1 = new EntryApplication(context, ep1)

      // These first to verify we are using the superclass audits from AbstractResultCalculator
      intercept[AssertionError] {
        dummy.calculateResults(context, Seq.empty, Seq(SimplePercentPriceRow(10)))
      }

      intercept[AssertionError] {
        dummy.calculateResults(context, Seq(Seq(new EntryApplication(context, ep1))), Seq.empty)
      }
      
      // Now verify we support only expected types in current class
      val ea2 = new ApplicationResult() {
        val step = Option(CalculationStepID(50))
        val depth = Option(1)
        val applies = true
        def messages = Nil
      }
      intercept[MatchError] {
        dummy.doCalculateResults(context, Seq((Seq(ea2), pr)))
      }
      
      val fpr = SimpleFixedPriceRow(price)
      intercept[ClassCastException] {
        dummy.doCalculateResults(context, Seq((Seq(ea1), fpr)))
      }
    }
    
    "not override a None price" in {
      val pr = SimplePercentPriceRow(10)
      val ea1 = new EntryApplication(context, SimpleEntryPrice(id = Option(new EntryPriceID()), entry = EntryID(1), quantity = Quantity(1, EACH)))
      
      val result1 = dummy.doCalculateResults(context, Seq((Seq(ea1), pr)))
      assertResult(1)(result1.size)
      assert(result1(0).isInstanceOf[EntryApplication])
      val expected1 = ea1.copy(
        toEntryPrice = result1(0).asInstanceOf[EntryApplication].toEntryPrice
      )
      assertResult(Seq(ea1))(result1)
    }
    
    "override a Some price" in {
      val pr = SimplePercentPriceRow(10)
      val ea1 = new EntryApplication(context, ep1)

      
      val result1 = dummy.doCalculateResults(context, Seq((Seq(ea1), pr)))
      assertResult(1)(result1.size)
      assert(result1(0).isInstanceOf[EntryApplication])
      val expected1 = ea1.copy(
        toEntryPrice = result1(0).asInstanceOf[EntryApplication].toEntryPrice,
        unitPrice = Option(price),
        applies = true
      )
      assertResult(Seq(expected1))(result1)
    }
  }
  
  "PercentIncreaseResultCalculator" should {
    val calculator = PercentIncreaseResultCalculator
    
    "accurately calculate unit price" in {
      val ea1 = new EntryApplication(context, ep1)
      assertResult(Money(9.9, USD))(calculator.calculatePrice(context, ea1, SimplePercentPriceRow(10), Money(9, USD)))
      assertResult(Money(0.02, USD))(calculator.calculatePrice(context, ea1, SimplePercentPriceRow(50), Money(0.01, USD)))
      assertResult(Money(0.01, USD))(calculator.calculatePrice(context, ea1, SimplePercentPriceRow(49), Money(0.01, USD)))
      assertResult(Money(1.58, USD))(calculator.calculatePrice(context, ea1, SimplePercentPriceRow(50), Money(1.05, USD)))
    }
    
    "accurately calculate results" in {
      val ea1 = new EntryApplication(context, ep1)
      val ea2 = new EntryApplication(context, ep2)
      val ea3 = new EntryApplication(context, ep3)
      
      val groups1 = Seq(Seq(ea1, ea2, ea3))
      val result1 = calculator.calculateResults(context, groups1, Seq(SimplePercentPriceRow(10)))
      assertResult(3)(result1.size)
      for { ea <- result1 } {
        ea match {
          case x: EntryApplication if x.fromEntryPrice == ea1.fromEntryPrice => 
            assertResult(ea1.copy(unitPrice = Option(Money(11, USD)), applies = true))(x)
          case x: EntryApplication if x.fromEntryPrice == ea2.fromEntryPrice => 
            assertResult(ea2.copy(unitPrice = Option(Money(22, USD)), applies = true))(x)
          case x: EntryApplication if x.fromEntryPrice == ea3.fromEntryPrice => 
            assertResult(ea3.copy(unitPrice = Option(Money(33, USD)), applies = true))(x)
        }
      }
      
      val groups2 = Seq(Seq(ea1), Seq(ea2, ea3))
      val result2 = calculator.calculateResults(context, groups2, Seq(SimplePercentPriceRow(5), SimplePercentPriceRow(20)))
      for { ea <- result2 } {
        ea match {
          case x: EntryApplication if x.fromEntryPrice == ea1.fromEntryPrice => 
            assertResult(ea1.copy(unitPrice = Option(Money(10.5, USD)), applies = true))(x)
          case x: EntryApplication if x.fromEntryPrice == ea2.fromEntryPrice => 
            assertResult(ea2.copy(unitPrice = Option(Money(24, USD)), applies = true))(x)
          case x: EntryApplication if x.fromEntryPrice == ea3.fromEntryPrice => 
            assertResult(ea3.copy(unitPrice = Option(Money(36, USD)), applies = true))(x)
        }
      }
    }
  }
  
  "PercentDecreaseResultCalculator" should {
    val calculator = PercentDecreaseResultCalculator
    
    "accurately calculate unit price" in {
      val ea1 = new EntryApplication(context, ep1)
      assertResult(Money(8.1, USD))(calculator.calculatePrice(context, ea1, SimplePercentPriceRow(10), Money(9, USD)))
      assertResult(Money(0.01, USD))(calculator.calculatePrice(context, ea1, SimplePercentPriceRow(50), Money(0.01, USD)))
      assertResult(Money(0.00, USD))(calculator.calculatePrice(context, ea1, SimplePercentPriceRow(51), Money(0.01, USD)))
      assertResult(Money(0.53, USD))(calculator.calculatePrice(context, ea1, SimplePercentPriceRow(50), Money(1.05, USD)))
    }
    
    "accurately calculate results" in {
      val ea1 = new EntryApplication(context, ep1)
      val ea2 = new EntryApplication(context, ep2)
      val ea3 = new EntryApplication(context, ep3)
      
      val groups1 = Seq(Seq(ea1, ea2, ea3))
      val result1 = calculator.calculateResults(context, groups1, Seq(SimplePercentPriceRow(10)))
      assertResult(3)(result1.size)
      for { ea <- result1 } {
        ea match {
          case x: EntryApplication if x.fromEntryPrice == ea1.fromEntryPrice => 
            assertResult(ea1.copy(unitPrice = Option(Money(9, USD)), applies = true))(x)
          case x: EntryApplication if x.fromEntryPrice == ea2.fromEntryPrice => 
            assertResult(ea2.copy(unitPrice = Option(Money(18, USD)), applies = true))(x)
          case x: EntryApplication if x.fromEntryPrice == ea3.fromEntryPrice => 
            assertResult(ea3.copy(unitPrice = Option(Money(27, USD)), applies = true))(x)
        }
      }
      
      val groups2 = Seq(Seq(ea1), Seq(ea2, ea3))
      val result2 = calculator.calculateResults(context, groups2, Seq(SimplePercentPriceRow(5), SimplePercentPriceRow(20)))
      for { ea <- result2 } {
        ea match {
          case x: EntryApplication if x.fromEntryPrice == ea1.fromEntryPrice => 
            assertResult(ea1.copy(unitPrice = Option(Money(9.5, USD)), applies = true))(x)
          case x: EntryApplication if x.fromEntryPrice == ea2.fromEntryPrice => 
            assertResult(ea2.copy(unitPrice = Option(Money(16, USD)), applies = true))(x)
          case x: EntryApplication if x.fromEntryPrice == ea3.fromEntryPrice => 
            assertResult(ea3.copy(unitPrice = Option(Money(24, USD)), applies = true))(x)
        }
      }
    }
  }
      
  "PercentPriceResultCalculator" should {
    val calculator = PercentPriceResultCalculator
    
    "accurately calculate unit price" in {
      val ea1 = new EntryApplication(context, ep1)
      assertResult(Money(0.9, USD))(calculator.calculatePrice(context, ea1, SimplePercentPriceRow(10), Money(9, USD)))
      assertResult(Money(0.01, USD))(calculator.calculatePrice(context, ea1, SimplePercentPriceRow(50), Money(0.01, USD)))
      assertResult(Money(0.01, USD))(calculator.calculatePrice(context, ea1, SimplePercentPriceRow(51), Money(0.01, USD)))
      assertResult(Money(0.00, USD))(calculator.calculatePrice(context, ea1, SimplePercentPriceRow(49), Money(0.01, USD)))
      assertResult(Money(0.53, USD))(calculator.calculatePrice(context, ea1, SimplePercentPriceRow(50), Money(1.05, USD)))
    }
    
    "accurately calculate results" in {
      val ea1 = new EntryApplication(context, ep1)
      val ea2 = new EntryApplication(context, ep2)
      val ea3 = new EntryApplication(context, ep3)
      
      val groups1 = Seq(Seq(ea1, ea2, ea3))
      val result1 = calculator.calculateResults(context, groups1, Seq(SimplePercentPriceRow(10)))
      assertResult(3)(result1.size)
      for { ea <- result1 } {
        ea match {
          case x: EntryApplication if x.fromEntryPrice == ea1.fromEntryPrice => 
            assertResult(ea1.copy(unitPrice = Option(Money(1, USD)), applies = true))(x)
          case x: EntryApplication if x.fromEntryPrice == ea2.fromEntryPrice => 
            assertResult(ea2.copy(unitPrice = Option(Money(2, USD)), applies = true))(x)
          case x: EntryApplication if x.fromEntryPrice == ea3.fromEntryPrice => 
            assertResult(ea3.copy(unitPrice = Option(Money(3, USD)), applies = true))(x)
        }
      }
      
      val groups2 = Seq(Seq(ea1), Seq(ea2, ea3))
      val result2 = calculator.calculateResults(context, groups2, Seq(SimplePercentPriceRow(5), SimplePercentPriceRow(20)))
      for { ea <- result2 } {
        ea match {
          case x: EntryApplication if x.fromEntryPrice == ea1.fromEntryPrice => 
            assertResult(ea1.copy(unitPrice = Option(Money(0.5, USD)), applies = true))(x)
          case x: EntryApplication if x.fromEntryPrice == ea2.fromEntryPrice => 
            assertResult(ea2.copy(unitPrice = Option(Money(4, USD)), applies = true))(x)
          case x: EntryApplication if x.fromEntryPrice == ea3.fromEntryPrice => 
            assertResult(ea3.copy(unitPrice = Option(Money(6, USD)), applies = true))(x)
        }
      }
    }
  }
}