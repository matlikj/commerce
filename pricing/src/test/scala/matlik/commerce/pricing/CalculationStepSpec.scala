package matlik.commerce.pricing

import org.joda.time.DateTime
import org.junit.runner.RunWith
import org.scalatest.WordSpec
import org.scalatest.Matchers
import org.scalacheck._
import org.scalacheck.Arbitrary._
import org.scalacheck.Gen._
import org.scalacheck.Prop._
import matlik.measures.Money
import matlik.measures.SimpleImplicits._
import matlik.commerce.Quantity
import matlik.commerce.Quantity._
import matlik.measures.SimpleImplicits

@RunWith(classOf[org.scalatest.junit.JUnitRunner])
class CalculationStepSpec extends WordSpec with Matchers {

  "CalculationStep object" should {

    "support filtering EntryPrice objects by restrictions" in {
      // All empty collections
      assertResult(Seq.empty)(CalculationStep.filterEntriesByRestrictions(Nil, Nil))

      // Populated entries but empty restrictions
      val ep1 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(1))
      val ep2 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(2))
      val ep3 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(3))
      val entryPrices = Seq(ep1, ep2, ep3)
      assert(entryPrices === CalculationStep.filterEntriesByRestrictions(entryPrices, Nil))

      // Filter with restrictions.. both denied (remove) and allowed (no-op)
      val calculationStepID = Option(CalculationStepID(999))
      val calculationDepth = Option(1)
      val r1a = EntryPriceAllowed(calculationStepID, calculationDepth, ep1.entry, ep1.id.get)
      val r2a = EntryPriceAllowed(calculationStepID, calculationDepth, ep2.entry, ep2.id.get)
      val r3a = EntryPriceAllowed(calculationStepID, calculationDepth, ep3.entry, ep3.id.get)
      val r1d = EntryPriceDenied(calculationStepID, calculationDepth, ep1.entry, ep1.id.get, Seq())
      val r2d = EntryPriceDenied(calculationStepID, calculationDepth, ep2.entry, ep2.id.get, Seq())
      val r3d = EntryPriceDenied(calculationStepID, calculationDepth, ep3.entry, ep3.id.get, Seq())

      assert(entryPrices === CalculationStep.filterEntriesByRestrictions(entryPrices, Seq(r2a)))
      assert(entryPrices === CalculationStep.filterEntriesByRestrictions(entryPrices, Seq(r2a, r1a, r3a)))

      assertResult(Seq(ep1, ep3))(CalculationStep.filterEntriesByRestrictions(entryPrices, Seq(r2d)))
      assertResult(Seq.empty)(CalculationStep.filterEntriesByRestrictions(entryPrices, Seq(r2d, r1d, r3d)))

      assertResult(Seq(ep1, ep3))(CalculationStep.filterEntriesByRestrictions(entryPrices, Seq(r1a, r2d, r3a)))
      assertResult(Seq(ep3))(CalculationStep.filterEntriesByRestrictions(entryPrices, Seq(r1a, r2d, r3a, r1d)))

      assertResult(Seq.empty)(CalculationStep.filterEntriesByRestrictions(Seq(), Seq(r1d, r2d)))
    }

    "support EntryPrice mutation by calculation result" in {
      val entryPrice = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(1))
      val calculationStepID = Option(CalculationStepID(999))
      val calculationDepth = Option(1)
      val mutation1 = EntryApplication(
        step = calculationStepID,
        depth = calculationDepth,
        entry = entryPrice.entry,
        fromEntryPrice = Seq(entryPrice.id.get),
        toEntryPrice = new EntryPriceID(),
        quantity = entryPrice.quantity,
        startDate = None,
        endDate = None,
        applies = false,
        unitPrice = Option(Money(5, USD)))

      // applies is false, so no-op
      val (mutated1, remainder1) = CalculationStep.mutateEntryPrice(Seq(entryPrice), mutation1)
      assert(None == mutated1)
      assert(Seq(entryPrice) === remainder1)

      // Apply a mutation
      val mutation2 = mutation1.copy(applies = true)
      val (mutated2, remainder2) = CalculationStep.mutateEntryPrice(Seq(entryPrice), mutation2)
      assert(Seq() === remainder2)
      assert(!mutated2.isEmpty)
      for (x <- mutated2) {
        assert(entryPrice.entry == x.entry)
        assert(entryPrice.quantity == x.quantity)
        assert(mutation1.toEntryPrice == x.id.get)
        assert(mutation1.unitPrice == x.unitPrice)
        assert(None == x.startDate)
        assert(None == x.endDate)
      }

      // Apply a partial mutation
      val mutation3 = mutation2.copy(quantity = Quantity(3, EACH))
      val (mutated3, remainder3) = CalculationStep.mutateEntryPrice(Seq(entryPrice), mutation3)
      assert(!mutated3.isEmpty)
      assert(!remainder3.isEmpty)
      for (x <- mutated3) {
        assert(mutation3.quantity === x.quantity)
      }
      for (x <- remainder3) {
        assert(Quantity(7, EACH) === x.quantity)
      }

      // Ensure date ranges are calculated properly
      val date1 = Option(new DateTime())
      val date2 = Option((new DateTime()).plusDays(1))
      val date3 = Option((new DateTime()).plusDays(2))
      val date4 = Option((new DateTime()).plusDays(3))

      val entryPrice4 = entryPrice.copy(startDate = date1, endDate = date3)
      val mutation4 = mutation2.copy(startDate = date2, endDate = date4)
      val (mutated4, remainder4) = CalculationStep.mutateEntryPrice(Seq(entryPrice4), mutation4)
      assert(Seq() === remainder4)
      for (x <- mutated4) {
        assertResult(date2)(x.startDate)
        assertResult(date3)(x.endDate)
      }

      val entryPrice5 = entryPrice.copy(startDate = date2, endDate = date4)
      val mutation5 = mutation2.copy(startDate = date1, endDate = date3)
      val (mutated5, remainder5) = CalculationStep.mutateEntryPrice(Seq(entryPrice4), mutation4)
      assert(Seq() === remainder5)
      for (x <- mutated4) {
        assertResult(date2)(x.startDate)
        assertResult(date3)(x.endDate)
      }
    }

    "support multiple EntryPrice mutation into one by calculation result" in {
      pending
    }

    "filter collections of EntryPrice using CalculationResults" in {

      assertResult((Seq.empty, Seq.empty))(CalculationStep.filterEntriesByCalculationResults(Nil, Nil))

      val entryPrice1 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(1))
      val entryPrice2 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(2))
      val entryPrice3 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(3))

      val view = Seq(entryPrice1, entryPrice2, entryPrice3)
      assertResult((Seq.empty, view))(CalculationStep.filterEntriesByCalculationResults(view, Nil))

      val calculationStepID = Option(CalculationStepID(999))
      val calculationDepth = Option(1)

      val mutation1 = EntryApplication(
        step = calculationStepID,
        depth = calculationDepth,
        entry = entryPrice1.entry,
        fromEntryPrice = Seq(entryPrice1.id.get),
        toEntryPrice = new EntryPriceID(),
        quantity = entryPrice1.quantity,
        startDate = None,
        endDate = None,
        applies = true,
        unitPrice = Option(Money(4, USD)))

      val mutation2 = EntryApplication(
        step = calculationStepID,
        depth = calculationDepth,
        entry = entryPrice2.entry,
        fromEntryPrice = Seq(entryPrice2.id.get),
        toEntryPrice = new EntryPriceID(),
        quantity = Quantity(4, EACH),
        startDate = None,
        endDate = None,
        applies = true,
        unitPrice = Option(Money(10, USD)))

      val restriction3 = StepDenied(calculationStepID, calculationDepth, Seq())

      val calculationResults = Seq(mutation1, mutation2, restriction3)
      val (consumed, available) = CalculationStep.filterEntriesByCalculationResults(view, calculationResults)

      assertResult(2)(available.size)
      val available1 = available.filter(_.entry === entryPrice1.entry)
      val available2 = available.filter(_.entry === entryPrice2.entry)
      val available3 = available.filter(_.entry === entryPrice3.entry)
      assert(available1.isEmpty)
      assert(!available2.isEmpty)
      assertResult(Quantity(6, EACH))(available2(0).quantity)
      assert(!available3.isEmpty)
      assertResult(entryPrice3.quantity)(available3(0).quantity)

      assertResult(2)(consumed.size)
      val consumed1 = consumed.filter(_.entry === entryPrice1.entry)
      val consumed2 = consumed.filter(_.entry === entryPrice2.entry)
      val consumed3 = consumed.filter(_.entry === entryPrice3.entry)
      assert(!consumed1.isEmpty)
      assertResult(entryPrice1.quantity)(consumed1(0).quantity)
      assert(!consumed2.isEmpty)
      assertResult(mutation2.quantity)(consumed2(0).quantity)
      assert(consumed3.isEmpty)

      val calculationResults2 = Seq(mutation1.copy(applies = false), mutation2.copy(applies = false), restriction3)
      val (consumedB, availableB) = CalculationStep.filterEntriesByCalculationResults(view, calculationResults2)
      assertResult(Seq.empty)(consumedB)
      assertResult(view)(availableB.sortBy(f => f.entry.asInstanceOf[EntryID].value))
    }
  }

  "CalculationStep" should {
    import CalculationStep._

    val entry1 = EntryID(1)
    val entry2 = EntryID(2)
    val entry3 = EntryID(3)

    "set a simple fixed price" in {
      val ep1 = SimpleEntryPrice(Option(new EntryPriceID()), entry1)
      val ep2 = SimpleEntryPrice(Option(new EntryPriceID()), entry2)
      val ep3 = SimpleEntryPrice(Option(new EntryPriceID()), entry3)
      val initial = Seq(ep1, ep2, ep3)

      val context = SimpleCalculationContext(stepView = initial)

      val priceRowSelector = new PriceRowSelector() {
        override def selectPriceRows(context: CalculationContext, groups: Seq[ApplicationResultGroup]): Seq[PriceRow] = {
          var value = 0
          for { group <- groups } yield {
            value = value + 10
            SimpleFixedPriceRow(Money(value, USD))
          }
        }
      }

      val entryRestrictor = new EntryPriceRestrictor {
        override def getEntryPriceRestrictions(context: CalculationContext): Seq[EntryPriceRestriction] = {
          val step = context.calculationStep
          val depth = context.calculationDepth
          context.stepView.map(e => if (e.entry === entry3) new EntryPriceDenied(step, depth, e) else new EntryPriceAllowed(step, depth, e))
        }
      }

      /* With single grouper */
      {
        val step1 = new ComposibleCalculationStep(
          stepID = CalculationStepID(0),
          grouper = SingleEntryGrouper,
          priceRowSelector = priceRowSelector,
          resultCalculator = FixedUnitPriceResultCalculator,
          stepRestrictor = None,
          entryRestrictor = Option(entryRestrictor))

        val resultSet1 = step1.calculate(context)(SimpleImplicits.SimpleConverter)
        assertResult(Seq(StepAllowed(Option(step1.id), Option(1)), StepAllowed(Option(step1.id), Option(2))))(resultSet1.filter(_.isInstanceOf[StepAllowed]))
        val entryRestrictions1 = resultSet1.filter(_.isInstanceOf[EntryPriceRestriction])
        assertResult(4)(entryRestrictions1.size)

        val denied1 = entryRestrictions1.filter(x => x.isInstanceOf[EntryPriceDenied])
        assertResult(1)(denied1.size)
        for { d <- denied1; e = d.asInstanceOf[EntryPriceDenied] } {
          assertResult(ep3.id.get)(e.entryPrice)
        }

        val allowed1 = entryRestrictions1.filter(_.isInstanceOf[EntryPriceAllowed]).map(_.asInstanceOf[EntryPriceAllowed])
        assertResult(1)(allowed1.count(x => x === new EntryPriceAllowed(Option(step1.id), Option(1), ep1)))
        assertResult(1)(allowed1.count(x => x === new EntryPriceAllowed(Option(step1.id), Option(1), ep2)))
        assertResult(1)(allowed1.count(x => x === new EntryPriceAllowed(Option(step1.id), Option(2), ep2)))

        val applied1 = resultSet1.filter(_.isInstanceOf[EntryApplication]).map(_.asInstanceOf[EntryApplication])
        val applied1ep1 = applied1.filter(x => x.fromEntryPrice(0) == ep1.id.get)
        assertResult(1)(applied1ep1.size)
        for (x <- applied1ep1) {
          assert(x.applies)
          assertResult(Option(step1.id))(x.step)
          assertResult(Option(1))(x.depth)
          assertResult(Option(Money(10, USD)))(x.unitPrice)
        }
        val applied1ep2 = applied1.filter(x => x.fromEntryPrice(0) == ep2.id.get)
        assertResult(1)(applied1ep2.size)
        for (x <- applied1ep2) {
          assert(x.applies)
          assertResult(Option(step1.id))(x.step)
          assertResult(Option(2))(x.depth)
          assertResult(Option(Money(10, USD)))(x.unitPrice)
        }
      }

      /* With batch grouper */
      {
        val step2 = new ComposibleCalculationStep(
          stepID = CalculationStepID(1),
          grouper = BatchEntryGrouper,
          priceRowSelector = priceRowSelector,
          resultCalculator = FixedUnitPriceResultCalculator,
          stepRestrictor = None,
          entryRestrictor = Option(entryRestrictor))

        val resultSet2 = step2.calculate(context)(SimpleImplicits.SimpleConverter)

        assertResult(Seq(StepAllowed(Option(step2.id), Option(1))))(resultSet2.filter(_.isInstanceOf[StepAllowed]))
        val entryRestrictions2 = resultSet2.filter(_.isInstanceOf[EntryPriceRestriction])
        assertResult(3)(entryRestrictions2.size)

        val denied2 = entryRestrictions2.filter(x => x.isInstanceOf[EntryPriceDenied])
        assertResult(1)(denied2.size)
        for { d <- denied2; e = d.asInstanceOf[EntryPriceDenied] } {
          assertResult(ep3.id.get)(e.entryPrice)
        }

        val allowed2 = entryRestrictions2.filter(_.isInstanceOf[EntryPriceAllowed]).map(_.asInstanceOf[EntryPriceAllowed])
        assertResult(1)(allowed2.count(x => x === new EntryPriceAllowed(Option(step2.id), Option(1), ep1)))
        assertResult(1)(allowed2.count(x => x === new EntryPriceAllowed(Option(step2.id), Option(1), ep2)))

        val applied2 = resultSet2.filter(_.isInstanceOf[EntryApplication]).map(_.asInstanceOf[EntryApplication])
        val applied2ep1 = applied2.filter(x => x.fromEntryPrice(0) == ep1.id.get)
        assertResult(1)(applied2ep1.size)
        for (x <- applied2ep1) {
          assert(x.applies)
          assertResult(Option(step2.id))(x.step)
          assertResult(Option(1))(x.depth)
          assertResult(Option(Money(10, USD)))(x.unitPrice)
        }
        val applied2ep2 = applied2.filter(x => x.fromEntryPrice(0) == ep2.id.get)
        assertResult(1)(applied2ep2.size)
        for (x <- applied2ep2) {
          assert(x.applies)
          assertResult(Option(step2.id))(x.step)
          assertResult(Option(1))(x.depth)
          assertResult(Option(Money(20, USD)))(x.unitPrice)
        }
      }
    }

    "calculate a simple percentage price adjustment" in {
      val ep1 = SimpleEntryPrice(Option(new EntryPriceID()), entry1, unitPrice = Option(Money(10, USD)))
      val ep2 = SimpleEntryPrice(Option(new EntryPriceID()), entry2, unitPrice = Option(Money(20, USD)))
      val ep3 = SimpleEntryPrice(Option(new EntryPriceID()), entry3, unitPrice = Option(Money(30, USD)))
      val initial = Seq(ep1, ep2, ep3)

      val context = SimpleCalculationContext(stepView = initial)

      val priceRowSelector = new PriceRowSelector() {
        override def selectPriceRows(context: CalculationContext, groups: Seq[ApplicationResultGroup]): Seq[PriceRow] = {
          for { group <- groups } yield {
            SimplePercentPriceRow(group(0).asInstanceOf[EntryApplication].unitPrice.get.qty.toDouble)
          }
        }
      }

      val step1 = new ComposibleCalculationStep(
        stepID = CalculationStepID(0),
        grouper = BatchEntryGrouper,
        priceRowSelector = priceRowSelector,
        resultCalculator = PercentIncreaseResultCalculator,
        stepRestrictor = None,
        entryRestrictor = None)

      val resultSet1 = step1.calculate(context)(SimpleImplicits.SimpleConverter)

      val applied = resultSet1.filter(_.isInstanceOf[EntryApplication]).map(_.asInstanceOf[EntryApplication])
      assert(3 === applied.size)

      val appliedEp1 = applied.filter(x => x.fromEntryPrice(0) == ep1.id.get)
      assertResult(1)(appliedEp1.size)
      for (x <- appliedEp1) {
        assert(x.applies)
        assertResult(Option(step1.id))(x.step)
        assertResult(Option(1))(x.depth)
        assertResult(Option(Money(11, USD)))(x.unitPrice)
      }

      val appliedEp2 = applied.filter(x => x.fromEntryPrice(0) == ep2.id.get)
      assertResult(1)(appliedEp2.size)
      for (x <- appliedEp2) {
        assert(x.applies)
        assertResult(Option(Money(24, USD)))(x.unitPrice)
      }

      val appliedEp3 = applied.filter(x => x.fromEntryPrice(0) == ep3.id.get)
      assertResult(1)(appliedEp3.size)
      for (x <- appliedEp3) {
        assert(x.applies)
        assertResult(Option(Money(39, USD)))(x.unitPrice)
      }
    }

    "calculate a simple fixed price adjustment" in {
      val ep1 = SimpleEntryPrice(Option(new EntryPriceID()), entry1, unitPrice = Option(Money(10, USD)))
      val ep2 = SimpleEntryPrice(Option(new EntryPriceID()), entry2, unitPrice = Option(Money(20, USD)))
      val ep3 = SimpleEntryPrice(Option(new EntryPriceID()), entry3, unitPrice = Option(Money(30, USD)))
      val initial = Seq(ep1, ep2, ep3)

      val context = SimpleCalculationContext(stepView = initial)

      val priceRowSelector = new PriceRowSelector() {
        override def selectPriceRows(context: CalculationContext, groups: Seq[ApplicationResultGroup]): Seq[PriceRow] = {
          for { group <- groups } yield {
            SimpleFixedPriceRow(Money(group(0).asInstanceOf[EntryApplication].unitPrice.get.qty.toDouble, USD))
          }
        }
      }

      val resultCalculator = new ComposibleDistributedFixedPriceResultCalculator(shareDistributor = PriceBasedShareDistribution, fixedUnitPriceCalculator = FixedUnitPriceUplift)

      val step1 = new ComposibleCalculationStep(
        stepID = CalculationStepID(0),
        grouper = BatchEntryGrouper,
        priceRowSelector = priceRowSelector,
        resultCalculator = resultCalculator,
        stepRestrictor = None,
        entryRestrictor = None)

      val resultSet1 = step1.calculate(context)(SimpleImplicits.SimpleConverter)

      val applied = resultSet1.filter(_.isInstanceOf[EntryApplication]).map(_.asInstanceOf[EntryApplication])
      assert(3 === applied.size)

      val appliedEp1 = applied.filter(x => x.fromEntryPrice(0) == ep1.id.get)
      assertResult(1)(appliedEp1.size)
      for (x <- appliedEp1) {
        assert(x.applies)
        assertResult(Option(step1.id))(x.step)
        assertResult(Option(1))(x.depth)
        assertResult(Option(Money(11, USD)))(x.unitPrice)
      }

      val appliedEp2 = applied.filter(x => x.fromEntryPrice(0) == ep2.id.get)
      assertResult(1)(appliedEp2.size)
      for (x <- appliedEp2) {
        assert(x.applies)
        assertResult(Option(Money(22, USD)))(x.unitPrice)
      }

      val appliedEp3 = applied.filter(x => x.fromEntryPrice(0) == ep3.id.get)
      assertResult(1)(appliedEp3.size)
      for (x <- appliedEp3) {
        assert(x.applies)
        assertResult(Option(Money(33, USD)))(x.unitPrice)
      }
    }

    "calculate a multi-product price adjustment" in {
      val ep1 = SimpleEntryPrice(Option(new EntryPriceID()), entry1, unitPrice = Option(Money(10, USD)), quantity = Quantity(2, EACH))
      val ep2 = SimpleEntryPrice(Option(new EntryPriceID()), entry2, unitPrice = Option(Money(20, USD)), quantity = Quantity(1, EACH))
      val ep3 = SimpleEntryPrice(Option(new EntryPriceID()), entry3, unitPrice = Option(Money(30, USD)), quantity = Quantity(1, EACH))
      val initial = Seq(ep1, ep2, ep3)

      val context = SimpleCalculationContext(stepView = initial)

      val grouper = new Grouper() {
        override def getApplicationGroups(context: CalculationContext): Seq[ApplicationResultGroup] = {
          // We are cheating a bit here.
          val e1 = context.stepView.filter(_.entry === entry1)
          val e3 = context.stepView.filter(_.entry === entry3)
          if (!e3.isEmpty) {
            val ea1 = new EntryApplication(context, ep1.copy(quantity = Quantity(1, EACH)))
            val ea3 = new EntryApplication(context, e3(0))
            Seq(Seq(ea1, ea3))
          } else Seq()
        }
      }

      val priceRowSelector = new PriceRowSelector() {
        override def selectPriceRows(context: CalculationContext, groups: Seq[ApplicationResultGroup]): Seq[PriceRow] = {
          for { group <- groups } yield {
            SimpleFixedPriceRow(Money(20, USD))
          }
        }
      }

      val resultCalculator = new ComposibleDistributedFixedPriceResultCalculator(shareDistributor = PriceBasedShareDistribution, fixedUnitPriceCalculator = FixedUnitPriceDiscount)

      val step1 = new ComposibleCalculationStep(
        stepID = CalculationStepID(0),
        grouper = grouper,
        priceRowSelector = priceRowSelector,
        resultCalculator = resultCalculator,
        stepRestrictor = None,
        entryRestrictor = None)

      val resultSet1 = step1.calculate(context)(SimpleImplicits.SimpleConverter)

      val applied = resultSet1.filter(_.isInstanceOf[EntryApplication]).map(_.asInstanceOf[EntryApplication])
      assert(2 === applied.size)

      val appliedEp1 = applied.filter(x => x.fromEntryPrice(0) == ep1.id.get)
      assertResult(1)(appliedEp1.size)
      for (x <- appliedEp1) {
        assert(x.applies)
        assertResult(Option(step1.id))(x.step)
        assertResult(Option(1))(x.depth)
        assertResult(Option(Money(5, USD)))(x.unitPrice)
        assertResult(Quantity(1, EACH))(x.quantity)
      }

      val appliedEp3 = applied.filter(x => x.fromEntryPrice(0) == ep3.id.get)
      assertResult(1)(appliedEp3.size)
      for (x <- appliedEp3) {
        assert(x.applies)
        assertResult(Option(step1.id))(x.step)
        assertResult(Option(1))(x.depth)
        assertResult(Option(Money(15, USD)))(x.unitPrice)
        assertResult(Quantity(1, EACH))(x.quantity)
      }
    }
  }
}
