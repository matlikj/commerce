package matlik.commerce.pricing

import org.junit.runner.RunWith
import org.scalatest.WordSpec
import org.scalatest.Matchers
import matlik.commerce.Quantity
import matlik.commerce.Quantity._
import matlik.commerce.QUnit
import matlik.measures.Money
import matlik.measures.SimpleImplicits._
import matlik.measures.SimpleImplicits
import scala.util.Random

@RunWith(classOf[org.scalatest.junit.JUnitRunner])
class MergeGroupersSpec extends WordSpec with Matchers {
  "MergeCriteria" should {
    val ONE = Quantity(1, EACH)
    val TWO = Quantity(2, EACH)
    val THREE = Quantity(3, EACH)
    val FOUR = Quantity(4, EACH)
    val FIVE = Quantity(5, EACH)

    val ep1 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(1), quantity = ONE)
    val ep2 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(2), quantity = THREE)
    val ep3 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(3), quantity = FIVE)

    "support extracting quantity values for consistent UOM" in {
      assertResult(Seq(1, 3, 5))(MergeCriteria.getQtyValues(EACH, Seq(ep1, ep2, ep3)))

      assertResult(Seq())(MergeCriteria.getQtyValues(EACH, Nil))

      object BOGUS extends QUnit {
        val longLabel = "Bogus"
        val shortLabel = "Bogus"
        val symbol = "Bogus"
        val significantDecimalDigits = 0
        val minimumValue = BigDecimal("1")
      }

      intercept[IllegalArgumentException] {
        MergeCriteria.getQtyValues(BOGUS, Seq(ep1, ep2, ep3))
      }

    }

    "support determination of quantity boundaries" in {
      assertResult(Seq())(MergeCriteria.determineQtyBoundary(Seq(), Seq(), Vector.empty))
      assertResult(Seq(1, 2, 3))(MergeCriteria.determineQtyBoundary(Seq(1, 2, 3), Seq(1, 2, 3), Vector.empty))
      assertResult(Seq(3, 1, 3, 1, 1, 1))(MergeCriteria.determineQtyBoundary(Seq(4, 5, 1), Seq(3, 4, 1, 2), Vector.empty))

      intercept[RuntimeException] {
        MergeCriteria.determineQtyBoundary(Seq(2), Seq(1), Vector.empty)
      }
    }

    "support expanding price sequences with compatible quantity boundaries" in {
      assertResult(Seq(ep1, ep2, ep3))(MergeCriteria.expandByQty(List(ep1, ep2, ep3), List(1, 3, 5), Vector.empty))
      assertResult(
        Seq(ep1,
          ep2.updateEntryPrice(quantity = TWO),
          ep2.updateEntryPrice(quantity = ONE),
          ep3.updateEntryPrice(quantity = FOUR),
          ep3.updateEntryPrice(quantity = ONE)))(MergeCriteria.expandByQty(List(ep1, ep2, ep3), List(1, 2, 1, 4, 1), Vector.empty))

      // Cannot explode a smaller qty into a larger distribution
      intercept[RuntimeException] {
        MergeCriteria.expandByQty(List(ep1, ep2), List(3, 1), Vector.empty)
      }

      // Distribution is too big
      intercept[RuntimeException] {
        MergeCriteria.expandByQty(List(ep1, ep2), List(1, 3, 1), Vector.empty)
      }

      // Distribution is too small
      intercept[RuntimeException] {
        MergeCriteria.expandByQty(List(ep1, ep2), List(1, 2), Vector.empty)
      }

    }

    "support cross sectioning of multiple sippable EntryPrice sequences" in {
      val ep1 = SimpleEntryPrice(None, EntryID(1))
      val ep2 = SimpleEntryPrice(None, EntryID(2))
      val ep3 = SimpleEntryPrice(None, EntryID(3))
      val ep4 = SimpleEntryPrice(None, EntryID(4))
      val ep5 = SimpleEntryPrice(None, EntryID(5))
      val ep6 = SimpleEntryPrice(None, EntryID(6))
      val ep7 = SimpleEntryPrice(None, EntryID(7))
      val ep8 = SimpleEntryPrice(None, EntryID(8))
      val ep9 = SimpleEntryPrice(None, EntryID(9))

      assertResult(
        Seq(Seq(ep1, ep4, ep7), Seq(ep2, ep5, ep8), Seq(ep3, ep6, ep9)))(MergeCriteria.toSeqCrossSections(
          Seq(Seq(ep1, ep2, ep3), Seq(ep4, ep5, ep6), Seq(ep7, ep8, ep9))))

      intercept[RuntimeException] {
        MergeCriteria.toSeqCrossSections(Seq(Seq(ep1, ep2, ep3), Seq(ep4, ep5), Seq(ep7, ep8, ep9)))
      }
    }
  }

  "EntryPriceOrdering" should {

    "support relative unit price ordering with UnitPriceOrdering" in {
      val ep1 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(1), unitPrice = Option(Money(1, USD)))
      val ep2 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(2), unitPrice = Option(Money(2, USD)))
      val ep3 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(3), unitPrice = None)

      assertResult(1)(UnitPriceOrdering.compare(ep2, ep1))
      assertResult(0)(UnitPriceOrdering.compare(ep1, ep2.updateEntryPrice(unitPrice = ep1.unitPrice)))
      assertResult(-1)(UnitPriceOrdering.compare(ep1, ep2))

      intercept[RuntimeException] {
        UnitPriceOrdering.compare(ep1, ep3)
      }
    }

    "support inverse relative unit price ordering with GreaterUnitPriceOrdering" in {
      val ep1 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(1), unitPrice = Option(Money(1, USD)))
      val ep2 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(2), unitPrice = Option(Money(2, USD)))
      val ep3 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(3), unitPrice = None)

      assertResult(-1)(GreaterUnitPriceOrdering.compare(ep2, ep1))
      assertResult(0)(GreaterUnitPriceOrdering.compare(ep1, ep2.updateEntryPrice(unitPrice = ep1.unitPrice)))
      assertResult(1)(GreaterUnitPriceOrdering.compare(ep1, ep2))

      intercept[RuntimeException] {
        GreaterUnitPriceOrdering.compare(ep1, ep3)
      }
    }

    "support relative quantity ordering with QuantityOrdering" in {
      object CASE extends QUnit {
        val longLabel = "Case"
        val shortLabel = "Case"
        val symbol = "Case"
        val significantDecimalDigits = 1
        val minimumValue = BigDecimal("1")
      }

      val ep1 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(1), quantity = Quantity(1, EACH))
      val ep2 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(2), quantity = Quantity(2, EACH))
      val ep3 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(3), quantity = Quantity(1, CASE))

      assertResult(1)(QuantityOrdering.compare(ep2, ep1))
      assertResult(0)(QuantityOrdering.compare(ep1, ep2.updateEntryPrice(quantity = ep1.quantity)))
      assertResult(-1)(QuantityOrdering.compare(ep1, ep2))

      intercept[AssertionError] {
        QuantityOrdering.compare(ep1, ep3)
      }
    }
  }

  val e1 = EntryID(1)
  val e2 = EntryID(2)
  val e3 = EntryID(3)

  val ep1a = SimpleEntryPrice(Option(new EntryPriceID()), e1, quantity = Quantity(3, EACH), unitPrice = Option(Money(1, USD)))
  val ep2a = SimpleEntryPrice(Option(new EntryPriceID()), e2, quantity = Quantity(3, EACH), unitPrice = Option(Money(2, USD)))
  val ep3a = SimpleEntryPrice(Option(new EntryPriceID()), e3, quantity = Quantity(3, EACH), unitPrice = Option(Money(3, USD)))

  val ep1b1 = SimpleEntryPrice(Option(new EntryPriceID()), e1, quantity = Quantity(1, EACH), unitPrice = Option(Money(0.5, USD)))
  val ep1b2 = SimpleEntryPrice(Option(new EntryPriceID()), e1, quantity = Quantity(2, EACH), unitPrice = Option(Money(1.5, USD)))
  val ep2b1 = SimpleEntryPrice(Option(new EntryPriceID()), e2, quantity = Quantity(2, EACH), unitPrice = Option(Money(2.25, USD)))
  val ep2b2 = SimpleEntryPrice(Option(new EntryPriceID()), e2, quantity = Quantity(1, EACH), unitPrice = Option(Money(2.75, USD)))
  val ep3b = SimpleEntryPrice(Option(new EntryPriceID()), e3, quantity = Quantity(3, EACH), unitPrice = Option(Money(4, USD)),
    startDate = ep3a.startDate.map(_.minusDays(1)), endDate = ep3a.startDate.map(_.plusDays(7)))

  val ep1c = SimpleEntryPrice(Option(new EntryPriceID()), e1, quantity = Quantity(3, EACH), unitPrice = Option(Money(1, USD)))
  val ep2c1 = SimpleEntryPrice(Option(new EntryPriceID()), e2, quantity = Quantity(1, EACH), unitPrice = Option(Money(10, USD)))
  val ep2c2 = SimpleEntryPrice(Option(new EntryPriceID()), e2, quantity = Quantity(1, EACH), unitPrice = Option(Money(20, USD)))
  val ep2c3 = SimpleEntryPrice(Option(new EntryPriceID()), e2, quantity = Quantity(1, EACH), unitPrice = Option(Money(30, USD)))
  val ep3c = SimpleEntryPrice(Option(new EntryPriceID()), e3, quantity = Quantity(3, EACH), unitPrice = Option(Money(5, USD)),
    startDate = ep3a.startDate.map(_.minusDays(7)), endDate = ep3a.startDate.map(_.plusDays(2)))

  val emptyContext = SimpleCalculationContext(Seq())

  "MergeCriteriaByRealtiveEntryPriceOrdering" should {

    "properly merge valid collections of EntryPrices" in {
      val merger = MergeCriteriaByRealtiveEntryPriceOrdering(GreaterUnitPriceOrdering)

      // Ensure you cannot merge two lists of matching entries with unequal quantity
      intercept[RuntimeException] {
        merger.groupByCriteria(emptyContext, Seq(Seq(ep1a), Seq(ep1b1)))
      }

      // Ensure you cannot merge with empty collection
      intercept[RuntimeException] {
        merger.groupByCriteria(emptyContext, Seq(Seq(ep1a), Seq()))
      }

      // Ensure you cannot merge when missing entry data
      intercept[RuntimeException] {
        merger.groupByCriteria(emptyContext, Seq(Seq(ep3a, ep1a), Seq(ep3b)))
      }

      val result = merger.groupByCriteria(emptyContext, Seq(Seq(ep1a, ep2a, ep3a), Seq(ep1b1, ep1b2, ep2b1, ep2b2, ep3b), Seq(ep3c, ep2c1, ep1c, ep2c2, ep2c3)))

      assert(result.size == 6)
      assert(result.forall(_.size == 3))

      for (crossSection <- result) {
        val entry = crossSection.head.entry
        val quantity = crossSection.head.quantity
        assert(crossSection.forall(_.entry == entry))
        assert(crossSection.forall(_.quantity == quantity))
        assert(3 == crossSection.map(_.id).toSet.size)

        import matlik.measures.SimpleImplicits.SimpleConverter
        val sumUnitPrice = crossSection.map(_.unitPrice.get).reduce(_ + _)

        if (entry == e1) {
          assert(Seq(Money(2.5, USD), Money(3.5, USD)).contains(sumUnitPrice))
        } else if (entry == e2) {
          assert(Seq(Money(14.25, USD), Money(24.25, USD), Money(34.75, USD)).contains(sumUnitPrice))
        } else if (entry == e3) {
          assert(Money(12, USD) == sumUnitPrice)
        } else {
          fail(s"Unexpected entry of ${entry}.  Expected one of ${e1}, ${e2}, ${e3}")
        }
      }
    }
  }

  "CalculationValueMerger" should {
    import matlik.measures.SimpleImplicits._

    val priceNameA = CalculationValueName("price name 1")
    val priceNameB = CalculationValueName("price name 2")
    val priceNameC = CalculationValueName("price name 3")

    val pricesA = Random.shuffle(Seq(ep1a, ep2a, ep3a))
    val pricesB = Random.shuffle(Seq(ep1b1, ep1b2, ep2b1, ep2b2, ep3b))
    val pricesC = Random.shuffle(Seq(ep1c, ep2c1, ep2c2, ep2c3, ep3c))

    val cva = CalculationValue(name = priceNameA, results = Seq(), prices = pricesA)
    val cvb = CalculationValue(name = priceNameB, results = Seq(), prices = pricesB)
    val cvc = CalculationValue(name = priceNameC, results = Seq(), prices = pricesC)

    val priceDocument = SimplePriceDocument(calculationValues = Seq(cva, cvb, cvc))
    val context = SimpleCalculationContext(stepView = Seq(), priceDocument = Some(priceDocument))

    "support grouping CalculationValue entry prices relative to unit price value" in {
      // Simply return the entries to group
      object dummyMergeCriteria extends MergeCriteria {
        def groupByCriteria(context: CalculationContext, toGroup: Seq[EntryPriceSeq]) = {
          toGroup
        }
      }

      val merger = CalculationValueSummingCalculationStep(CalculationStepID(1), Seq(priceNameA, priceNameB, priceNameC), dummyMergeCriteria)

      val extractedToGroup = merger.groupForMerge(context)

      assertResult(Seq(pricesA, pricesB, pricesC))(extractedToGroup)
    }

    "support summing entry prices" in {

      val merger = CalculationValueSummingCalculationStep(CalculationStepID(1), Nil, null)

      val x1 = SimpleEntryPrice(Option(new EntryPriceID()), e1, unitPrice = Option(Money(1, USD)),
        startDate = Option(Const.BLACK_FRIDAY), endDate = Option(Const.BLACK_FRIDAY.plusDays(7)))
      val x2 = SimpleEntryPrice(Option(new EntryPriceID()), e1, unitPrice = Option(Money(3, USD)),
        startDate = Option(Const.BLACK_FRIDAY.minusDays(2)), endDate = Option(Const.BLACK_FRIDAY.plusDays(2)))
      val x3 = SimpleEntryPrice(Option(new EntryPriceID()), e1, unitPrice = Option(Money(5, USD)),
        startDate = Option(Const.BLACK_FRIDAY.minusDays(7)), endDate = Option(Const.BLACK_FRIDAY.plusDays(5)))

      // Test the valid scenario
      {
        val result = merger.doMerge(context, Seq(x1, x2, x3))
        assert(result.applies, result)
        assert(result.messages.isEmpty)
        assert(e1 === result.entry)
        assert(Option(Money(9, USD)) === result.unitPrice)
        assert(Option(Const.BLACK_FRIDAY) === result.startDate)
        assert(Option(Const.BLACK_FRIDAY.plusDays(2)) === result.endDate)
        assert(result.toEntryPrice !== x1.id)
        assert(result.toEntryPrice !== x2.id)
        assert(result.toEntryPrice !== x3.id)
        assert(result.fromEntryPrice.contains(x1.id.get))
        assert(result.fromEntryPrice.contains(x2.id.get))
        assert(result.fromEntryPrice.contains(x3.id.get))
        assert(result.quantity === x1.quantity)
      }

      // Test the invalid date scenario
      {
        val result = merger.doMerge(context, Seq(x1, x2, x3.copy(endDate = Option(Const.BLACK_FRIDAY.minusDays(6)))))
        assert(!result.applies)
        assert(!result.messages.isEmpty)
      }
    }

    "support summing entry prices across multiple CalculationValues where entry and entry unit price are equal" in {
      val merger = CalculationValueSummingCalculationStep(CalculationStepID(1), Seq(priceNameA, priceNameB, priceNameC),
        MergeCriteriaByRealtiveEntryPriceOrdering(GreaterUnitPriceOrdering))

      val prices = merger.calculate(context)
      assert(6 === prices.size)
      val e1Prices = prices.collect { case ea: EntryApplication if ea.entry === e1 => ea }
      val e2Prices = prices.collect { case ea: EntryApplication if ea.entry === e2 => ea }
      val e3Prices = prices.collect { case ea: EntryApplication if ea.entry === e3 => ea }
      assert(2 === e1Prices.size)
      assert(e1Prices.map(_.unitPrice).flatten.contains(Money(2.5, USD)))
      assert(e1Prices.map(_.unitPrice).flatten.contains(Money(3.5, USD)))
      assert(3 === e2Prices.size)
      assert(e2Prices.map(_.unitPrice).flatten.contains(Money(14.25, USD)))
      assert(e2Prices.map(_.unitPrice).flatten.contains(Money(24.25, USD)))
      assert(e2Prices.map(_.unitPrice).flatten.contains(Money(34.75, USD)))
      assert(1 === e3Prices.size)
      assert(e3Prices.map(_.unitPrice).flatten.contains(Money(12, USD)))
    }
  }
}