package matlik.commerce.pricing

import org.joda.time.DateTime
import org.junit.runner.RunWith
import org.scalatest.WordSpec
import org.scalatest.Matchers
import matlik.measures._
import matlik.measures.SimpleImplicits._
import matlik.commerce.Quantity
import matlik.commerce.Quantity._
import matlik.commerce.pk._
import matlik.commerce.pricing.CalculationStep._
import matlik.commerce.pricing.PriceCalculator._
import matlik.common.Message
import PriceCalculatorExamples._
import scala.collection.mutable
import scala.language.implicitConversions

@RunWith(classOf[org.scalatest.junit.JUnitRunner])
class PriceCalculatorSpec extends WordSpec with Matchers {
  import SimpleCalculationContextConverter._

  "PriceCalculator" should {

    "calculate base price successfully" in {
      val e1 = EntryID(1)
      val e2 = EntryID(2)
      val e3 = EntryID(3)
      val ctx1 = SimpleCalculationContextConverter.build(e1, e2, e3).copy(testLevel = 0)
      val result = pricer.calculate(ctx1)(SimpleImplicits.SimpleConverter)
      assert(result != null)
      assert(result.calculationValues != null)
      assertResult(1)(result.calculationValues.size)
      assertResult(3)(result.calculationValues.head.prices.size)
      val prices1 = result.getValue(CV_BASE_PRICE).get.getPrices(e1)
      assert(prices1.size == 1)
      checkPrice(prices1, 1)
      val prices2 = result.getValue(CV_BASE_PRICE).get.getPrices(e2)
      assert(prices2.size == 1)
      checkPrice(prices2, 2)
      val prices3 = result.getValue(CV_BASE_PRICE).get.getPrices(e3)
      assert(prices3.size == 1)
      checkPrice(prices3, 3)

      // Make sure the Discounted price equals the base price (no discounts) with testLevel 0
      assertResult(None)(result.getValue(CV_DISCOUNTED))
    }
  }

  "return previous CalculationValue results when the requested CalculationValue is active but no adjustments apply" in {
    val e1 = EntryID(1)
    val e2 = EntryID(2)
    val e3 = EntryID(3)
    val ctx1 = SimpleCalculationContextConverter.build(e1, e2, e3).copy(testLevel = 1)
    val result = pricer.calculate(ctx1)(SimpleImplicits.SimpleConverter)
    assert(result != null)
    assert(result.calculationValues != null)
    assertResult(2)(result.calculationValues.size)
    assertResult(3)(result.getValue(CV_BASE_PRICE).get.prices.size)
    assertResult(3)(result.getValue(CV_DISCOUNTED).get.prices.size)
    assertResult(result.getValue(CV_BASE_PRICE).get.getPrices(e1))(result.getValue(CV_DISCOUNTED).get.getPrices(e1))
    assertResult(result.getValue(CV_BASE_PRICE).get.getPrices(e2))(result.getValue(CV_DISCOUNTED).get.getPrices(e2))
    assertResult(result.getValue(CV_BASE_PRICE).get.getPrices(e3))(result.getValue(CV_DISCOUNTED).get.getPrices(e3))
  }

  "calculate dependent CalculationValue successfully" in {
    import SimpleCalculationContextConverter._
    val e1 = EntryID(1)
    val e2 = EntryID(2)
    val e3 = EntryID(3)
    val ctx1 = SimpleCalculationContextConverter.build(e1, e2, e3).copy(testLevel = 2)
    val result = pricer.calculate(ctx1)(SimpleImplicits.SimpleConverter)

    val basePriceCV = result.getValue(CV_BASE_PRICE).get
    assertResult(3)(basePriceCV.prices.size)
    assertResult(1)(checkPrice(basePriceCV.getPrices(e1), 1).size)
    assertResult(1)(checkPrice(basePriceCV.getPrices(e2), 2).size)
    assertResult(1)(checkPrice(basePriceCV.getPrices(e3), 3).size)

    val discountCV = result.getValue(CV_DISCOUNTED).get
    assertResult(3)(discountCV.prices.size)
    assertResult(1)(checkPrice(discountCV.getPrices(e1), 0.9).size)
    assertResult(1)(checkPrice(discountCV.getPrices(e2), 2).size)
    assertResult(1)(checkPrice(discountCV.getPrices(e3), 2.7).size)

    assertResult(basePriceCV.getPrices(e2))(discountCV.getPrices(e2))
  }

  "calculate partial quantity price adjustment honoring stacking" in {
    val e5 = EntryID(5)
    val e7 = EntryID(7)
    val e10 = EntryID(10)
    val e15 = EntryID(15)
    val entries = Seq((e5, 2), (e7, 2), (e10, 3), (e15, 1))
    val ctx = SimpleCalculationContextConverter.buildWithSeq(entries).copy(testLevel = 3)
    val result = pricer.calculate(ctx)(SimpleImplicits.SimpleConverter)

    val basePriceCV = result.getValue(CV_BASE_PRICE).get
    assertResult(4)(basePriceCV.prices.size)
    checkPrice(basePriceCV.getPrices(e5), 5, 2)
    checkPrice(basePriceCV.getPrices(e7), 7, 2)
    checkPrice(basePriceCV.getPrices(e10), 10, 3)
    checkPrice(basePriceCV.getPrices(e15), 15)

    val discountCV = result.getValue(CV_DISCOUNTED).get
    assertResult(6)(discountCV.prices.size)

    assert(discountCV.getPrices(e5).size == 2)
    assertResult(1)(checkPrice(discountCV.getPrices(e5), 2.5).size)
    assertResult(1)(checkPrice(discountCV.getPrices(e5), 4.5).size)

    assert(discountCV.getPrices(e7).size == 1)
    assertResult(1)(checkPrice(discountCV.getPrices(e7), 6.3, 2).size)

    assert(discountCV.getPrices(e10).size == 2)
    assertResult(1)(checkPrice(discountCV.getPrices(e10), 5, 1).size)
    assertResult(1)(checkPrice(discountCV.getPrices(e10), 10, 2).size)

    assert(discountCV.getPrices(e15).size == 1)
    assertResult(1)(checkPrice(discountCV.getPrices(e15), 7.5).size)

    assert(discountCV.getMessages(e7).isEmpty)
    assertResult(Seq(BundleDiscountStepSuccessMessage, BundleDiscountStepFailMessage))(discountCV.getMessages(e5))
    assertResult(Seq(BundleDiscountStepSuccessMessage, BundleDiscountStepFailMessage))(discountCV.getMessages(e10))
    assertResult(Seq(BundleDiscountStepSuccessMessage))(discountCV.getMessages(e15))
  }

  "support calculation of free values" in {
    val e5 = EntryID(5)
    val e7 = EntryID(7)
    val e10 = EntryID(10)
    val e15 = EntryID(15)
    val entries = Seq((e5, 2), (e7, 2), (e10, 3), (e15, 1))
    val ctx = SimpleCalculationContextConverter.buildWithSeq(entries).copy(testLevel = 10)
    val result = pricer.calculate(ctx)(SimpleImplicits.SimpleConverter)

    val shippingCV = result.getValue(CV_SHIPPING).get
    assertResult(6)(shippingCV.prices.size)

    assert(shippingCV.getPrices(e5).size == 2)
    assertResult(2)(checkPrice(shippingCV.getPrices(e5), 2).size)

    assert(shippingCV.getPrices(e7).size == 1)
    assertResult(1)(checkPrice(shippingCV.getPrices(e7), 2, 2).size)

    assert(shippingCV.getPrices(e10).size == 2)
    assertResult(1)(checkPrice(shippingCV.getPrices(e10), 2, 1).size)
    assertResult(1)(checkPrice(shippingCV.getPrices(e10), 2, 2).size)

    assert(shippingCV.getPrices(e15).size == 1)
    assertResult(1)(checkPrice(shippingCV.getPrices(e15), 0).size)
  }

  "calculate values dependent upon multiple previous CalculationValues" in {
    // Taxes determined against total of discounted value and shipping/handling

    val e5 = EntryID(5)
    val e7 = EntryID(7)
    val entries = Seq((e5,2), (e7, 1))
    val ctx1 = SimpleCalculationContextConverter.buildWithSeq(entries).copy(testLevel = 13)
    val result = pricer.calculate(ctx1)(SimpleImplicits.SimpleConverter)
    assert(result != null)

    val subtotalCV = result.getValue(CV_SUBTOTAL).get
    val taxesCV = result.getValue(CV_TAX).get
    val grossCV = result.getValue(CV_GROSS).get
    
    assert(2 === subtotalCV.prices.size)
    assert(1 === subtotalCV.getPrices(e5).size)
    assert(1 === subtotalCV.getPrices(e7).size)
    assert(1 === checkPrice(subtotalCV.getPrices(e5), 6.5, 2).size)
    assert(1 === checkPrice(subtotalCV.getPrices(e7), 8.3, 1).size)
    
    assert(2 === taxesCV.prices.size)
    assert(1 === taxesCV.getPrices(e5).size)
    assert(1 === taxesCV.getPrices(e7).size)
    assert(1 === checkPrice(taxesCV.getPrices(e5), 0.65, 2).size)
    assert(1 === checkPrice(taxesCV.getPrices(e7), 0.83, 1).size)
    
    assert(2 === grossCV.prices.size)
    assert(1 === grossCV.getPrices(e5).size)
    assert(1 === grossCV.getPrices(e7).size)
    assert(1 === checkPrice(grossCV.getPrices(e5), 7.15, 2).size)
    assert(1 === checkPrice(grossCV.getPrices(e7), 9.13, 1).size)
  }

  def checkPrice(prices: Seq[EntryPrice], price: Double, qty: Int = 1): Seq[EntryPrice] = {
    val p = Money(price, USD)
    val q = Quantity(qty, EACH)
    val found = prices.filter(x => x.quantity == q && x.unitPrice == Option(p))
    if (found.isEmpty) {
      sys.error(s"Could not find entry with unitPrice $p and quantity $q in $prices")
    }
    found
  }

}



