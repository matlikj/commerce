package matlik.commerce.pk

import matlik.core.PK

trait AddressPK extends PK
trait ProductPK extends PK
trait ContactPK extends PK
trait EntryPK extends PK
trait UserPK extends PK

trait SalesDocumentPK { this: PK => }
trait CartPK extends PK with SalesDocumentPK
trait OrderPK extends PK with SalesDocumentPK


//case class AddressID(value: Long) extends PKLong
//case class ContactID(value: Long) extends PKLong
//case class SalesDocumentID(value: Long) extends PKLong
//case class UserID(value: Long) extends PKLong
